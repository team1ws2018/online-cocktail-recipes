INSERT INTO `cocktails`.`app_user_role`(`type`) VALUES ('ROLE_ADMIN');
INSERT INTO `cocktails`.`app_user_role`(`type`) VALUES ('ROLE_USER');

INSERT INTO `cocktails`.`visibility_control`(`visibility_control_type`) VALUES ('PUBLIC');
INSERT INTO `cocktails`.`visibility_control`(`visibility_control_type`) VALUES ('RESTRICTED');
INSERT INTO `cocktails`.`visibility_control`(`visibility_control_type`) VALUES ('PRIVATE');

INSERT INTO `cocktails`.`app_user`(`password`, `username`, `email`, `blocked`, `blocked_until`,`email_notification`) VALUES ('admin', 'admin', 'admin@email.com', false, null, false);
INSERT INTO `cocktails`.`app_user`(`password`, `username`, `email`, `blocked`, `blocked_until`,`email_notification`) VALUES ('user', 'user', 'user@email.com', false, null, false);
INSERT INTO `cocktails`.`app_user`(`password`, `username`, `email`, `blocked`, `blocked_until`,`email_notification`) VALUES ('milan', 'milan', 'milan@email.com', false, null, false);
INSERT INTO `cocktails`.`app_user`(`password`, `username`, `email`, `blocked`, `blocked_until`,`email_notification`) VALUES ('jovan', 'jovan', 'jovan@email.com', false, null, false);
INSERT INTO `cocktails`.`app_user`(`password`, `username`, `email`, `blocked`, `blocked_until`,`email_notification`) VALUES ('milica', 'milica', 'milica@email.com', false, null, false);

INSERT INTO `cocktails`.`app_user_roles`(`app_user_id`, `app_user_role_id`) VALUES (1,1);
INSERT INTO `cocktails`.`app_user_roles`(`app_user_id`, `app_user_role_id`) VALUES (1,2);
INSERT INTO `cocktails`.`app_user_roles`(`app_user_id`, `app_user_role_id`) VALUES (2,2);
INSERT INTO `cocktails`.`app_user_roles`(`app_user_id`, `app_user_role_id`) VALUES (3,2);
INSERT INTO `cocktails`.`app_user_roles`(`app_user_id`, `app_user_role_id`) VALUES (4,2);
INSERT INTO `cocktails`.`app_user_roles`(`app_user_id`, `app_user_role_id`) VALUES (5,2);


