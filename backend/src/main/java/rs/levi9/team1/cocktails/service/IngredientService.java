package rs.levi9.team1.cocktails.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.levi9.team1.cocktails.domain.Ingredient;
import rs.levi9.team1.cocktails.repository.IngredientRepository;

import java.util.List;

@Transactional
@Service
public class IngredientService {

    private IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    } // end constructor

    public List<Ingredient> findAll() {
        return ingredientRepository.findAll();
    } // end findAll

    public List<Ingredient> findAllByCocktailId(Long id) {
        return ingredientRepository.findAllByCocktailId(id);
    } // end findAllByCocktailId

    public Ingredient findOne(Long id) {
        return ingredientRepository.findOne(id);
    } // end findOne

    public Ingredient save(Ingredient ingredient) {
        return ingredientRepository.save(ingredient);
    } // end save

    public void delete(Long id) {
        ingredientRepository.delete(id);
    } // end delete

    public void deleteAll(List<Ingredient> ingredients) {
        ingredientRepository.delete(ingredients);
    }

    public void deleteByCocktailId(Long id) {
        ingredientRepository.deleteByCocktailId(id);
    }



} // end class
