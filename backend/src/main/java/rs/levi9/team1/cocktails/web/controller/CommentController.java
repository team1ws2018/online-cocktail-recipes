package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.team1.cocktails.domain.AppUser;
import rs.levi9.team1.cocktails.domain.Cocktail;
import rs.levi9.team1.cocktails.domain.Comment;
import rs.levi9.team1.cocktails.domain.Report;
import rs.levi9.team1.cocktails.service.CocktailService;
import rs.levi9.team1.cocktails.service.CommentService;
import rs.levi9.team1.cocktails.service.EmailService;
import rs.levi9.team1.cocktails.service.ReportService;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("comments")
@CrossOrigin(origins = "http://localhost:4200")
public class CommentController {

    private CommentService commentService;
    private EmailService emailService;
    private CocktailService cocktailService;
    private ReportService reportService;

    @Autowired
    public CommentController(CommentService commentService, EmailService emailService, CocktailService cocktailService, ReportService reportService) {
        this.commentService = commentService;
        this.emailService = emailService;
        this.cocktailService = cocktailService;
        this.reportService = reportService;
    } // end constructor

//    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity findAllComments() {
        List<Comment> comments = this.commentService.findAll();
        if (comments == null)
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);

        return new ResponseEntity(comments, HttpStatus.OK);
    } // end findAllComments

//    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public ResponseEntity findAllCommentsByCocktailId(@PathVariable("id") Long id) {
        List<Comment> comments = this.commentService.findAllByCocktailId(id);
        if (comments == null)
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);

        return new ResponseEntity(comments, HttpStatus.OK);
    } // end findAllCommentsByCocktailId

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity save(@RequestBody Comment comment) {
        Comment result = commentService.save(comment);
        if (result == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        Cocktail commentedCocktail = cocktailService.findOne(comment.getCocktailId());
        if (commentedCocktail == null) {
            return new ResponseEntity(comment, HttpStatus.OK);
        }
        AppUser commentedCocktailUser = commentedCocktail.getAppUser();
        if (commentedCocktailUser == null) {
            return new ResponseEntity(comment, HttpStatus.OK);
        } else if(commentedCocktailUser.getEmailNotification()) {
            String email = commentedCocktailUser.getEmail();
            String subject = "Social activity";
            String message = "Dear " + commentedCocktailUser.getUsername() + ", <br><br>" +
                    result.getAppUser().getUsername() + " commented on your " +
                    commentedCocktail.getCocktailName() + " cocktail: <br><blockquote>\"" + comment.getCommentText() +
                    "\"</blockquote> <br><br>Online Cocktail Recipes";
            try {
            emailService.sendEmail(email, subject, message);
        } // end try
        catch (MessagingException e) {
            System.err.println(e);
        } // end catch
            return new ResponseEntity(comment, HttpStatus.OK);
        } else {
        return new ResponseEntity(comment, HttpStatus.OK);
        }
    } // end save

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity update(@Valid @RequestBody Comment comment) {

        Comment commentToUpdate = commentService.findOne(comment.getId());

        if (Objects.isNull(commentToUpdate)) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        //update comment
        commentToUpdate.setCommentText(comment.getCommentText());
        commentToUpdate.setCommentPostDate(new Date());
        commentService.update(commentToUpdate);

        //send email only if user exist and wants to recive email notification
        Cocktail commentedCocktail = cocktailService.findOne(comment.getCocktailId());
        if (commentedCocktail == null) {
            return new ResponseEntity(HttpStatus.OK);
        } else if (commentedCocktail.getAppUser() == null) {
            return new ResponseEntity(HttpStatus.OK);
        } else if (commentedCocktail.getAppUser().getEmailNotification()) {
            String email = commentedCocktail.getAppUser().getEmail();
            String subject = "Social activity";
            String message = "Dear " + commentedCocktail.getAppUser().getUsername() + ", <br><br>" +
                    comment.getAppUser().getUsername() + " edited his/her comment on your " +
                    commentedCocktail.getCocktailName() + " cocktail: <br><blockquote>\"" + comment.getCommentText() +
                    "\"</blockquote> <br><br>Online Cocktail Recipes";
            try {
                emailService.sendEmail(email, subject, message);
            } // end try
            catch (MessagingException e) {
                System.err.println(e);
            } // end catch

            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.OK);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        Report report = reportService.findByReportedCommentId(id);
        if (report == null) {
            commentService.delete(id);
        } else {
            report.setReportedCommentId(null);
            reportService.save(report);
            commentService.delete(id);
        }


    } // end delete



} // end CommentsController
