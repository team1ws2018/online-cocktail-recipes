package rs.levi9.team1.cocktails.domain;

import javax.persistence.Entity;

@Entity
public class Notification extends BaseEntity {

    private String notificationText;

    private Long notificationCocktailId;

    private String notificationCocktailName;

    private String notificationUsername;

    private Long userId;

    public Notification() {

    } // end constructor


    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public Long getNotificationCocktailId() {
        return notificationCocktailId;
    }

    public void setNotificationCocktailId(Long notificationCocktailId) {
        this.notificationCocktailId = notificationCocktailId;
    }

    public String getNotificationUsername() {
        return notificationUsername;
    }

    public void setNotificationUsername(String notificationUsername) {
        this.notificationUsername = notificationUsername;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNotificationCocktailName() {
        return notificationCocktailName;
    }

    public void setNotificationCocktailName(String notificationCocktailName) {
        this.notificationCocktailName = notificationCocktailName;
    }
} // end Report
