package rs.levi9.team1.cocktails.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.levi9.team1.cocktails.domain.Flag;
import rs.levi9.team1.cocktails.repository.FlagRepository;

import java.util.List;

@Transactional
@Service
public class FlagService {

    private FlagRepository flagRepository;

    @Autowired
    public FlagService(FlagRepository flagRepository) {
        this.flagRepository = flagRepository;
    } // end constructor

    public List<Flag> findAll() {
        return flagRepository.findAll();
    }

    public List<Flag> findAllByAppUserId(Long id) {
        return flagRepository.findAllByAppUserId(id);
    }

    public Flag findByAppUserIdAndCocktailId(Long uid, Long cid) {
        return flagRepository.findByAppUserIdAndCocktailId(uid, cid);
    }

    public List<Flag> findAllTriedFlags(Long uid) {
        return flagRepository.findByAppUserIdAndTried(uid, true);
    }

    public Flag save(Flag flag) {
        return flagRepository.save(flag);
    }

    public Flag update(Flag flag) {
        return flagRepository.save(flag);
    }

    public void delete(Long id) {
        flagRepository.delete(id);
    }

    public void deleteFlagsFromUser(Long id) {
        flagRepository.deleteAllByAppUserId(id);
    }
}
