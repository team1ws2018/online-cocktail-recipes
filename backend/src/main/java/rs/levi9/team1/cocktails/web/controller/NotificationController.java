package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.team1.cocktails.domain.AppUser;
import rs.levi9.team1.cocktails.domain.Notification;
import rs.levi9.team1.cocktails.service.AppUserService;
import rs.levi9.team1.cocktails.service.EmailService;
import rs.levi9.team1.cocktails.service.NotificationService;

import javax.mail.MessagingException;
import java.util.List;

@RestController
@RequestMapping("notifications")
@CrossOrigin(origins = "http://localhost:4200")
public class NotificationController {

    private NotificationService notificationService;
    private AppUserService userService;
    private EmailService emailService;
    @Autowired
    public NotificationController(NotificationService notificationService, AppUserService userService, EmailService emailService) {
        this.notificationService = notificationService;
        this.userService = userService;
        this.emailService = emailService;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity save(@RequestBody Notification notification) {
        Notification notificationToSave = notificationService.save(notification);
        if (notificationToSave == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        AppUser user = userService.findOne(notification.getUserId());
        if (user.getEmailNotification()) {
            try {
                String msg = notification.getNotificationUsername() + " " + notification.getNotificationText() + " " + notification.getNotificationCocktailName();
                emailService.sendEmail(user.getEmail(), "Notification", msg);
            } catch (MessagingException e) {
                System.err.println(e);
            }
        }
        return new ResponseEntity(notificationToSave, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "user/{userId}", method = RequestMethod.GET)
    public ResponseEntity getUserNOtification(@PathVariable("userId") Long userId) {
        List<Notification> userNotifications = notificationService.findAllUsersNotification(userId);
        if (userNotifications == null) {
            new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(userNotifications, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        notificationService.delete(id);
    }

}