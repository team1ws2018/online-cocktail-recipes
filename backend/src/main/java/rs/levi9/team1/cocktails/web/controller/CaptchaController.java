package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.levi9.team1.cocktails.service.CaptchaService;

@RestController
@RequestMapping("/captcha")
@CrossOrigin(origins = "http://localhost:4200")
public class CaptchaController {

    private CaptchaService captchaService;

    @Autowired
    public CaptchaController(CaptchaService captchaService) {
        this.captchaService = captchaService;
    }

    @GetMapping("/{code}")
    public boolean checkCaptcha(@PathVariable("code") String code){
        return captchaService.post(code, null).contains("true");
    }
}
