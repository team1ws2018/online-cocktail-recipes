package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.team1.cocktails.domain.Cocktail;
import rs.levi9.team1.cocktails.domain.Ingredient;
import rs.levi9.team1.cocktails.service.CocktailService;
import rs.levi9.team1.cocktails.service.IngredientService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("ingredients")
@CrossOrigin(origins = "http://localhost:4200")
public class IngredientController {

    private IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    } // end constructor

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.GET)
    public List<Ingredient> findAllIngredients() {
        return this.ingredientService.findAll();
    } // end findAllCocktails

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "cocktail/{id}" , method = RequestMethod.GET)
    public List<Ingredient> findAllByCocktailId(@PathVariable("id") Long id) {
        return ingredientService.findAllByCocktailId(id);
    } // end findAllByAppUserId

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public ResponseEntity findOne(@PathVariable("id") Long id) {
        Ingredient ingredient = ingredientService.findOne(id);
        if (ingredient == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        return new ResponseEntity(ingredient, HttpStatus.OK);
    } // end findById

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST)
    public Ingredient save(@RequestBody Ingredient ingredient) {
        return ingredientService.save(ingredient);
    } // end save

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.PUT)
    public Ingredient update(@Valid @RequestBody Ingredient ingredient) {
        return ingredientService.save(ingredient);
    } // end update

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        ingredientService.delete(id);
    } // end delete

} // end class
