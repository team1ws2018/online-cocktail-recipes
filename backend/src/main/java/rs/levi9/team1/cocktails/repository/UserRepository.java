package rs.levi9.team1.cocktails.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.levi9.team1.cocktails.domain.AppUser;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Long> {

    AppUser findByUsername(String username);

    AppUser findByUsernameOrEmail(String username, String email);

    AppUser findByEmail(String email);

    List<AppUser> findAllByBlocked(Boolean blocked);

    Long countByBlocked(boolean blocked);

} // end interface
