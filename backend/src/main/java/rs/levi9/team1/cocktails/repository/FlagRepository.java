package rs.levi9.team1.cocktails.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.levi9.team1.cocktails.domain.Flag;

import java.util.List;

@Repository
public interface FlagRepository extends JpaRepository<Flag, Long> {

    List<Flag> findAllByAppUserId(Long id);

    Flag findByAppUserIdAndCocktailId(Long uid, Long cid);

    List<Flag> findByAppUserIdAndTried(Long uid, boolean tried);

    void deleteAllByAppUserId(Long userId);


}
