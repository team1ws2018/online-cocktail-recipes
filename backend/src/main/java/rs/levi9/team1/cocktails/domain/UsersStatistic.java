package rs.levi9.team1.cocktails.domain;

public class UsersStatistic {

    private Long numberOfAllUsers;

    private Long numberOfAllActiveUsers;

    private Long numberOfAllBlockedUsers;

    public UsersStatistic(Long numberOfAllUsers, Long numberOfAllActiveUsers, Long numberOfAllBlockedUsers) {
        this.numberOfAllUsers = numberOfAllUsers;
        this.numberOfAllActiveUsers = numberOfAllActiveUsers;
        this.numberOfAllBlockedUsers = numberOfAllBlockedUsers;
    }

    public Long getNumberOfAllUsers() {
        return numberOfAllUsers;
    }

    public void setNumberOfAllUsers(Long numberOfAllUsers) {
        this.numberOfAllUsers = numberOfAllUsers;
    }

    public Long getNumberOfAllActiveUsers() {
        return numberOfAllActiveUsers;
    }

    public void setNumberOfAllActiveUsers(Long numberOfAllActiveUsers) {
        this.numberOfAllActiveUsers = numberOfAllActiveUsers;
    }

    public Long getNumberOfAllBlockedUsers() {
        return numberOfAllBlockedUsers;
    }

    public void setNumberOfAllBlockedUsers(Long numberOfAllBlockedUsers) {
        this.numberOfAllBlockedUsers = numberOfAllBlockedUsers;
    }
}
