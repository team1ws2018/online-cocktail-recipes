package rs.levi9.team1.cocktails.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.levi9.team1.cocktails.domain.Rating;

import java.util.List;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

    Rating findByAppUserIdAndCocktailId(Long uid, Long cid);

    List<Rating> findAllByAppUserId(Long id);

    void deleteAllByAppUserId(Long id);

    @Query("SELECT AVG(r.userRating) from Rating r where r.cocktailId = :id")
    Double averageCocktailRating(@Param("id") Long id);

}
