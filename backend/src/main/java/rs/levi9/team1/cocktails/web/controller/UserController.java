package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import rs.levi9.team1.cocktails.domain.*;
import rs.levi9.team1.cocktails.service.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("users")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    private AppUserService appUserService;
    private EmailService emailService;
    private CocktailService cocktailService;
    private IngredientService ingredientsService;
    private RatingService ratingService;
    private CommentService commentService;
    private FlagService flagService;

    @Autowired
    public UserController(AppUserService appUserService, EmailService emailService, CocktailService cocktailService
            , IngredientService ingredientsService, RatingService ratingService, CommentService commentService, FlagService flagService) {
        this.appUserService = appUserService;
        this.emailService = emailService;
        this.cocktailService = cocktailService;
        this.ingredientsService = ingredientsService;
        this.ratingService = ratingService;
        this.commentService = commentService;
        this.flagService = flagService;
    } // end constructor

    @RequestMapping("user")
    public AuthenticatedUser getUser(Authentication authentication) {
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            roles.add(authority.getAuthority());
        } // end for
        AuthenticatedUser user = new AuthenticatedUser(authentication.getName(), roles);
        return user;
    } // end getUser

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.GET)
    public List<AppUser> findAll() {
        return appUserService.findAll();
    } // end findAll

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public ResponseEntity findOne(@PathVariable("id") Long id) {
        AppUser appUser = appUserService.findOne(id);
        if (appUser == null)
            return new ResponseEntity("User is already in use!", HttpStatus.NOT_FOUND);

        return new ResponseEntity(appUser, HttpStatus.OK);
    } // return findOne

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity save(@Valid @RequestBody AppUser appUser) {
        AppUser userToSave = appUserService.findByUsernameOrEmail(appUser.getUsername(), appUser.getEmail());
        String verificationLink = "<h3>Please verify your registration by clicking on link below:</h3> <br>" +
                "<a href='http://localhost:8080/users/verification/" + appUser.getUsername() +
                "'> Verify your account </a>";
        if (userToSave == null) {
            userToSave = appUser;
            userToSave.setBlocked(true);
            try {
                emailService.sendEmail(userToSave.getEmail(), "Account", verificationLink);
            } catch (MessagingException e) {
                System.err.println(e);
            }
            appUserService.save(userToSave);
            return new ResponseEntity(userToSave, HttpStatus.OK);
        } // end if
        else {
            return new ResponseEntity("User is already in use!", HttpStatus.NOT_FOUND);
        } // end else
    } // end save

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        deleteAllCocktailsFromUser(id);
        deleteAllUserComments(id);
        deleteAllUserRatings(id);
        deleteAllUserFlags(id);
        appUserService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    } // end delete

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity update(@Valid @RequestBody AppUser appUser) {
        AppUser userToUpdate = this.appUserService.save(appUser);
        if (userToUpdate == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(userToUpdate, HttpStatus.OK);
    } // end update

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(path = "block-unblock", method = RequestMethod.PUT)
    public AppUser blockOrUnblockAppUser(@Valid @RequestBody AppUser appUser) {
        AppUser appUserToUpdate = appUserService.findOne(appUser.getId());
        if (appUserToUpdate.getBlocked()) {
            appUserToUpdate.setBlocked(false);
        } else {
            appUserToUpdate.setBlocked(true);
        } // end else

        if (appUserToUpdate.getBlocked()) {
            try {
                emailService.sendEmail(appUserToUpdate.getEmail(), "Account", "Your account has been blocked." +
                        "\n\nOnline Cocktail Recipes");
            } // end try
            catch (MessagingException e) {
                System.err.println(e);
            } // end catch
        } // end if
        else {
            try {
                emailService.sendEmail(appUserToUpdate.getEmail(), "Account", "Your account has been unblocked." +
                        "\n\nOnline Cocktail Recipes");
            } // end try
            catch (MessagingException e) {
                System.err.println(e);
            } // end catch
        }
        return appUserService.save(appUserToUpdate);
    }

    @RequestMapping(path = "verification/{username}")
    public ModelAndView onAccountVerification(@PathVariable("username") String username) {
        AppUser userToVerify = appUserService.findByUsername(username);
        userToVerify.setBlocked(false);
        appUserService.save(userToVerify);
        ModelAndView redirectUrl = new ModelAndView("redirect:" + "http://localhost:4200/#/login");

        return redirectUrl;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "/user/{username}", method = RequestMethod.GET)
    public ResponseEntity findByUsername(@PathVariable("username") String username) {
        AppUser user = appUserService.findByUsername(username);
        if (user == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        return new ResponseEntity(user, HttpStatus.OK);
    } // end findByUsername
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "/delete/all", method = RequestMethod.POST)
    public ResponseEntity deleteAll(@RequestBody List<AppUser> users) {
        users.forEach(user -> {
            deleteAllUserComments(user.getId());
            deleteAllUserRatings(user.getId());
            deleteAllCocktailsFromUser(user.getId());
            deleteAllUserFlags(user.getId());
        });
        appUserService.deleteAll(users);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(path = "statistics", method = RequestMethod.GET)
    public ResponseEntity getStatistics() {
        Long numberOfAllUsers = appUserService.countAllUsers();
        Long numbersOfActiveUsers = appUserService.countAllActiveUser();
        Long numberOfBlockedUsers = appUserService.countAllBlockedUsers();
        UsersStatistic statistics = new UsersStatistic(numberOfAllUsers, numbersOfActiveUsers, numberOfBlockedUsers);
        return new ResponseEntity(statistics, HttpStatus.OK);
    }

    //helper method for deleting all cocktails from user
    private void deleteAllCocktailsFromUser(Long userId) {
        List<Cocktail> userCocktailsToDelete = cocktailService.findAllByAppUserId(userId);
        System.err.println(userCocktailsToDelete);
        if (userCocktailsToDelete.size() > 0 && userCocktailsToDelete != null) {
            userCocktailsToDelete.forEach(cocktail -> deleteAllIngredientsFromCocktail(cocktail.getId()));
            cocktailService.deleteAll(userCocktailsToDelete);
        }
    }

    //helper method for deleting all ingredient from cocktail
    private void deleteAllIngredientsFromCocktail(Long cocktailId) {
        List<Ingredient> cocktailIngredientsToDelete = ingredientsService.findAllByCocktailId(cocktailId);
        System.err.println(cocktailIngredientsToDelete);
        if (cocktailIngredientsToDelete.size() > 0 && cocktailIngredientsToDelete != null) {
            ingredientsService.deleteAll(cocktailIngredientsToDelete);
        }

    }

    //helper method for deleting ratings from user
    private void deleteAllUserRatings(Long userId) {
        ratingService.deleteAllRatingsFromUser(userId);
    }

    //helper method for deleting ratings from user
    private void deleteAllUserComments(Long userId) {
        commentService.deleteAllCommentsFromUser(userId);
    }

    //helper method for deleting ratings from user
    private void deleteAllUserFlags(Long userId) {
        flagService.deleteFlagsFromUser(userId);
    }

} // end UserController
