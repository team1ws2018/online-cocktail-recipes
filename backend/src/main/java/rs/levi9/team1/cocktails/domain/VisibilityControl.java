package rs.levi9.team1.cocktails.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class VisibilityControl extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private VisibilityControlType visibilityControlType;

    public VisibilityControl() {
        this.visibilityControlType = VisibilityControlType.PUBLIC;
    } // end constructor

    public VisibilityControl(Long id, VisibilityControlType visibilityControlType) {
        super(id);
        this.visibilityControlType = visibilityControlType;
    } // end constructor

    public VisibilityControlType getVisibilityControlType() {
        return visibilityControlType;
    }

    public void setVisibilityControlType(VisibilityControlType visibilityControlType) {
        this.visibilityControlType = visibilityControlType;
    }

    public static enum VisibilityControlType {
        PUBLIC, RESTRICTED, PRIVATE
    } // end VisibilityControlType

} // end VisibilityControl
