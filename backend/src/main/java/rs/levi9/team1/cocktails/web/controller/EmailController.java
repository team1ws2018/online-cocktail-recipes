package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.team1.cocktails.domain.AppUser;
import rs.levi9.team1.cocktails.service.AppUserService;
import rs.levi9.team1.cocktails.service.EmailService;

import javax.mail.MessagingException;
import java.util.Objects;

@RestController
@RequestMapping(name = "email")
@CrossOrigin(origins = "http://localhost:4200")
public class EmailController {

    private EmailService emailService;
    private AppUserService appUserService;

    @Autowired
    public EmailController(EmailService emailService, AppUserService appUserService) {
        this.emailService = emailService;
        this.appUserService = appUserService;
    } // end constructor

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity sendEmail(@RequestBody AppUser appUser) {
        String messageText = "Thank you for registering on Online Cocktail Recipes!";
        String subject = "Registration";
        try {
            emailService.sendEmail(appUser.getEmail(), subject, messageText);
            return new ResponseEntity(HttpStatus.OK);
        } catch (MessagingException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    } // end sendEmail

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "recovery", method = RequestMethod.POST)
    public ResponseEntity sendForgottenUsernameAndPassword(@RequestBody String email) {
        AppUser appUser = appUserService.findByEmail(email);
        if (Objects.isNull(appUser)) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        String messageText = "Username: " + appUser.getUsername() + "\nPassword: " + appUser.getPassword();
        String subject = "Forgotten username and/or password";
        try {
            emailService.sendEmail(appUser.getEmail(), subject, messageText);
            return new ResponseEntity(HttpStatus.OK);
        } catch (MessagingException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    } // end sendForgottenUsernameAndPassword

} // end class
