package rs.levi9.team1.cocktails.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.levi9.team1.cocktails.domain.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
    Report findByReportedCommentId(Long commentId);
} // end ReportRepository
