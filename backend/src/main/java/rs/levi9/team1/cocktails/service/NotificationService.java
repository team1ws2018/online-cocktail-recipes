package rs.levi9.team1.cocktails.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.levi9.team1.cocktails.domain.Flag;
import rs.levi9.team1.cocktails.domain.Notification;
import rs.levi9.team1.cocktails.repository.FlagRepository;
import rs.levi9.team1.cocktails.repository.NotificationRepository;

import java.util.List;

@Transactional
@Service
public class NotificationService {

    private NotificationRepository notificationRepository;

    @Autowired
    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public Notification save(Notification notification) {
        return notificationRepository.save(notification);
    }

    public void delete(Long id) {
        notificationRepository.delete(id);
    }

    public List<Notification> findAllUsersNotification(Long userId) {
        return notificationRepository.findAllByUserId(userId);
    }
 }
