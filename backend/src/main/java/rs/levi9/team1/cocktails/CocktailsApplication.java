package rs.levi9.team1.cocktails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CocktailsApplication {
    public static void main(String[] args) {
        SpringApplication.run(CocktailsApplication.class, args);
    }
}
