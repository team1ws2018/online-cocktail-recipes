package rs.levi9.team1.cocktails.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.levi9.team1.cocktails.domain.Comment;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByCocktailIdOrderByCommentPostDateDesc(Long id);
    void deleteAllByAppUserId(Long id);
} // end interface
