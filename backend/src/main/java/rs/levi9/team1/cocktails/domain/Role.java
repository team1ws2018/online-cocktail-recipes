package rs.levi9.team1.cocktails.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "app_user_role")
public class Role extends BaseEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private RoleType type;

    public Role() {
        this.type = RoleType.ROLE_USER;
    } // end constructor

    public Role(Long id, RoleType type) {
        super(id);
        this.type = type;
    } // end constructor

    public RoleType getType() {
        return type;
    } // end getter getType

    public void setType(RoleType type) {
        this.type = type;
    } // end setter setType

    public enum RoleType {
        ROLE_USER, ROLE_ADMIN
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Role role = (Role) o;

        return type == role.type;
    } // end equals

    @Override
    public int hashCode() {
        return type.hashCode();
    } // end hashCode

    @Override
    public String toString() {
        return "Role{" +
                "type=" + type +
                "}";
    } // end toString


} // end class
