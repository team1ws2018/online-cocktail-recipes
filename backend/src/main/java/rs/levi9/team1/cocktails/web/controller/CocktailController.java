package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.team1.cocktails.domain.*;
import rs.levi9.team1.cocktails.service.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("cocktails")
@CrossOrigin(origins = "http://localhost:4200")
public class CocktailController {

    private CocktailService cocktailService;
    private IngredientService ingredientService;

    @Autowired
    public CocktailController(CocktailService cocktailService, IngredientService ingredientService) {
        this.cocktailService = cocktailService;
        this.ingredientService = ingredientService;
    } // end constructor


    @RequestMapping(method = RequestMethod.GET)
    public List<Cocktail> findAllCocktails() {
        return this.cocktailService.findAll();
    } // end findAllCocktails


    @RequestMapping(path = "user/{id}" , method = RequestMethod.GET)
    public List<Cocktail> findAllByAppUserId(@PathVariable("id") Long id) {
        return cocktailService.findAllByAppUserId(id);
    } // end findAllByAppUserId


    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public ResponseEntity findOne(@PathVariable("id") Long id) {
        Cocktail cocktail = cocktailService.findOne(id);
        if (cocktail == null)
            return new ResponseEntity(null, HttpStatus.OK);

        return new ResponseEntity(cocktail, HttpStatus.OK);
    } // end findById

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity save(@RequestBody Cocktail cocktail) {
        Cocktail cocktailToSave = cocktailService.save(cocktail);
        return new ResponseEntity(cocktailToSave, HttpStatus.OK);
    } // end save

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity update(@Valid @RequestBody Cocktail cocktail) {
        Cocktail cocktailToUpdate = cocktailService.save(cocktail);
        if (cocktailToUpdate == null) {
            return new ResponseEntity(cocktailToUpdate, HttpStatus.OK);
        }
        return new ResponseEntity(cocktailToUpdate, HttpStatus.OK);
    } // end update

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        ingredientService.deleteByCocktailId(id);
        cocktailService.delete(id);
    } // end delete


    @RequestMapping(path = "statistics/user/{id}", method = RequestMethod.GET)
    public ResponseEntity getUserStatistics(@PathVariable("id") Long id) {
        Long numberOfUserCocktails = cocktailService.countCocktailsFromUser(id);
        CocktailsStatistic statistics = new CocktailsStatistic(numberOfUserCocktails);
        return new ResponseEntity(statistics, HttpStatus.OK);
    }


    @RequestMapping(path = "statistics", method = RequestMethod.GET)
    public ResponseEntity getStatistics() {
        Long numberOfAllCocktails = cocktailService.countAllCocktails();
        CocktailsStatistic statistics = new CocktailsStatistic(numberOfAllCocktails);
        return new ResponseEntity(statistics, HttpStatus.OK);
    }



    @RequestMapping(path = "search/category/{category}", method = RequestMethod.GET)
    ResponseEntity findCocktailsByCategory(@PathVariable("category") String category) {
        String search = category.trim().replaceAll("%20", " ");
        List<Cocktail> cocktails = cocktailService.findCocktailsByCategory(search);
        if (cocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(cocktails, HttpStatus.OK);
    }


    @RequestMapping(path = "search/type/{cocktailType}", method = RequestMethod.GET)
    ResponseEntity findCocktailsByType(@PathVariable("cocktailType") String cocktailType) {
        String search = cocktailType.trim().replaceAll("%20", " ");
        List<Cocktail> cocktails = cocktailService.findCocktailsByType(search);
        if (cocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(cocktails, HttpStatus.OK);
    }


    @RequestMapping(path = "search/glass/{glassType}", method = RequestMethod.GET)
    ResponseEntity findCocktailsByGlassType(@PathVariable("glassType") String glassType) {
        String search = glassType.trim().replaceAll("%20", " ");
        List<Cocktail> cocktails = cocktailService.findCocktailsByGlassType(search);
        if (cocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(cocktails, HttpStatus.OK);
    }


    @RequestMapping(path = "search/name/{searchQuery}", method = RequestMethod.GET)
    ResponseEntity findCocktailsByName(@PathVariable("searchQuery") String searchQuery) {
        String search = searchQuery.trim().replaceAll("%20", " ");
        List<Cocktail> cocktails = cocktailService.findCocktailsByName(search);
        if (cocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(cocktails, HttpStatus.OK);
    }

    @RequestMapping(path = "search/public", method = RequestMethod.GET)
    ResponseEntity findPublicCocktails() {
        List<Cocktail> cocktails = cocktailService.findPublicCocktails();
        if (cocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(cocktails, HttpStatus.OK);
    }

    @RequestMapping(path = "search/public/category/{category}", method = RequestMethod.GET)
    ResponseEntity findPublicCocktailsByCategory(@PathVariable("category") String category) {
        String search = category.trim().replaceAll("%20", " ");
        List<Cocktail> cocktails = cocktailService.findPublicCocktailsByCategory(search);
        if (cocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(cocktails, HttpStatus.OK);
    }

    @RequestMapping(path = "search/public/type/{cocktailType}", method = RequestMethod.GET)
    ResponseEntity findPublicCocktailsByType(@PathVariable("cocktailType") String cocktailType) {
        String search = cocktailType.trim().replaceAll("%20", " ");
        List<Cocktail> cocktails = cocktailService.findPublicCocktailsByType(search);
        if (cocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(cocktails, HttpStatus.OK);
    }

    @RequestMapping(path = "search/public/glass/{glassType}", method = RequestMethod.GET)
    ResponseEntity findPublicCocktailsByGlassType(@PathVariable("glassType") String glassType) {
        String search = glassType.trim().replaceAll("%20", " ");
        List<Cocktail> cocktails = cocktailService.findPublicCocktailsByGlassType(search);
        if (cocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(cocktails, HttpStatus.OK);
    }

    @RequestMapping(path = "search/public/name/{searchQuery}", method = RequestMethod.GET)
    ResponseEntity findPublicCocktailsByName(@PathVariable("searchQuery") String searchQuery) {
        String search = searchQuery.trim().replaceAll("%20", " ");
        List<Cocktail> cocktails = cocktailService.findPublicCocktailsByName(search);
        if (cocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(cocktails, HttpStatus.OK);
    }

} // end class
