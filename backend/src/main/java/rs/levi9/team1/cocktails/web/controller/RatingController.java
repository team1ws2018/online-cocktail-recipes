package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.team1.cocktails.domain.Cocktail;
import rs.levi9.team1.cocktails.domain.Rating;
import rs.levi9.team1.cocktails.service.CocktailService;
import rs.levi9.team1.cocktails.service.EmailService;
import rs.levi9.team1.cocktails.service.RatingService;

import javax.mail.MessagingException;
import java.util.List;

@RestController
@RequestMapping("ratings")
@CrossOrigin(origins = "http://localhost:4200")
public class RatingController {

    private RatingService ratingService;
    private CocktailService cocktailService;
    private EmailService emailService;

    @Autowired
    public RatingController(RatingService ratingService, CocktailService cocktailService) {
        this.ratingService = ratingService;
        this.cocktailService = cocktailService;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity  findAllRatings() {
        List<Rating> ratings = ratingService.findAll();
        if (ratings == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(ratings, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity saveRating(@RequestBody Rating rating) {
        Rating ratingToSave = ratingService.save(rating);
        if (ratingToSave == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        Cocktail ratedCocktail = cocktailService.findOne(rating.getCocktailId());
        if (ratedCocktail == null) {
            return new ResponseEntity(ratingToSave, HttpStatus.OK);
        }
        if (ratedCocktail.getAppUser().getEmailNotification()) {
            String email = ratedCocktail.getAppUser().getEmail();
            String subject = "Social activity";
            String message = "Dear " + ratedCocktail.getAppUser().getUsername() + ", <br><br>"+ rating.getAppUser().getUsername() +" rated your " +
                ratedCocktail.getCocktailName() + " cocktail.<br><br>Online Cocktail Recipes";
            try {
                emailService.sendEmail(email, subject, message);
            } // end try
            catch (MessagingException e) {
                System.err.println(e);
            } // end catch
            return new ResponseEntity(ratingToSave, HttpStatus.OK);
        } else {
            return new ResponseEntity(ratingToSave, HttpStatus.OK);
        }
    } // end cocktailRating

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity updateRating(@RequestBody Rating rating) {
        Rating ratingToSave = ratingService.save(rating);
        if (ratingToSave == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(ratingToSave, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    public void deleteRating(@PathVariable("id") Long id) {
        ratingService.delete(id);
    }


    @RequestMapping(path = "cocktail/{cocktailId}", method = RequestMethod.GET)
    public ResponseEntity averageCocktailRating(@PathVariable("cocktailId") Long id) {
    Double averageRating = ratingService.averageCocktailRating(id);
    return new ResponseEntity(averageRating, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "cocktail/{cocktailId}/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity userCocktailRating(@PathVariable("cocktailId") Long cocktailId, @PathVariable("userId") Long userId) {
        Rating rating = ratingService.findByAppUserIdAndCocktailId(userId, cocktailId);
        return new ResponseEntity(rating, HttpStatus.OK);
    }
}
