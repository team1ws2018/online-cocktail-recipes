package rs.levi9.team1.cocktails.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.levi9.team1.cocktails.domain.AppUser;
import rs.levi9.team1.cocktails.domain.Role;
import rs.levi9.team1.cocktails.repository.UserRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Transactional
@Service
public class AppUserService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public AppUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    } // end constructor

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            AppUser user = userRepository.findByUsername(username);
            if (user == null)
                return null;

            return new User(user.getUsername(), user.getPassword(), getAuthorities(user) );
        } // end try
        catch (Exception e) {
            throw new UsernameNotFoundException("User not found!");
        } // end catch
    } // end loadUserByUsername

    private Set<GrantedAuthority> getAuthorities(AppUser user) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (Role role : user.getRoles()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getType().toString());
            authorities.add(grantedAuthority);
        } // end for
        return authorities;
    } // end getAuthorities

    public List<AppUser> findAll() {
        return userRepository.findAll();
    } // end findAll

    public AppUser findOne(Long id) {
        return userRepository.findOne(id);
    } // end findOne

    public AppUser save(AppUser appUser) {
        return userRepository.save(appUser);
    } // end save

    public void delete(Long id) {
        userRepository.delete(id);
    } // end delete

    public AppUser findByUsernameOrEmail(String username, String email) {
        return userRepository.findByUsernameOrEmail(username, email);
    } // end findByUsernameOrEmail

    public AppUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    } // end findByUsername

    public AppUser findByEmail(String email) {
        return userRepository.findByEmail(email);
    } // end findByEmail

    public List<AppUser> findAllBlockedUsers() {
        return userRepository.findAllByBlocked(true);
    }

    public void deleteAll(List<AppUser> users) {
        userRepository.delete(users);
    }

    public Long countAllUsers() {
        return userRepository.count();
    }

    public Long countAllBlockedUsers() {
        return userRepository.countByBlocked(true);
    }

    public Long countAllActiveUser() {
        return userRepository.countByBlocked(false);
    }

} // end AppUserService
