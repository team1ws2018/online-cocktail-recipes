package rs.levi9.team1.cocktails.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Rating extends BaseEntity {

    @NotNull
    private Long cocktailId;

    @OneToOne
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;

    @NotNull
    private int userRating;

    public Rating() {

    } // end constructor


    public Long getCocktailId() {
        return cocktailId;
    } // end getter getCocktailId

    public void setCocktailId(Long cocktailId) {
        this.cocktailId = cocktailId;
    } // end setter setCocktailId

    public AppUser getAppUser() {
        return appUser;
    } // end getter getAppUser

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    } // end setter setAppUser

    public int getUserRating() {
        return userRating;
    } // end getter getUserRating

    public void setUserRating(int userRating) {
        this.userRating = userRating;
    } // end setter setUserRating


} // end class
