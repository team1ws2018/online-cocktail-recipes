package rs.levi9.team1.cocktails.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Flag extends BaseEntity {

    @NotNull
    private Long cocktailId;

    @OneToOne
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;

    private boolean tried = false;

    public Flag() {

    } // end constructor

    public Flag(boolean tried) {
        this.tried = tried;
    } // end constructor

    public Long getCocktailId() {
        return cocktailId;
    }

    public void setCocktailId(Long cocktailId) {
        this.cocktailId = cocktailId;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public boolean isTried() {
        return tried;
    }

    public void setTried(boolean tried) {
        this.tried = tried;
    }
} // end Flag
