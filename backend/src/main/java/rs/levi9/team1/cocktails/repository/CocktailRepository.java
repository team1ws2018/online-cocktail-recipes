package rs.levi9.team1.cocktails.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.levi9.team1.cocktails.domain.Cocktail;
import rs.levi9.team1.cocktails.domain.VisibilityControl;

import java.util.List;

public interface CocktailRepository extends JpaRepository<Cocktail, Long> {

    List<Cocktail> findAllByAppUserId(Long id);

    Long countByAppUserId(Long id);

    List<Cocktail> findAllByCocktailCategory(String category);

    List<Cocktail> findAllByCocktailType(String cocktailType);

    List<Cocktail> findAllByGlassType(String glassType);

    List<Cocktail> findAllByCocktailNameContainingIgnoreCase(String searchQuery);

    List<Cocktail> findAllByVisibilityControlVisibilityControlType(VisibilityControl.VisibilityControlType visibility);

    List<Cocktail> findAllByCocktailCategoryAndVisibilityControlVisibilityControlType(String category, VisibilityControl.VisibilityControlType visibility);

    List<Cocktail> findAllByCocktailTypeAndVisibilityControlVisibilityControlType(String cocktailType, VisibilityControl.VisibilityControlType visibility);

    List<Cocktail> findAllByGlassTypeAndVisibilityControlVisibilityControlType(String glassType, VisibilityControl.VisibilityControlType visibility);

    List<Cocktail> findAllByCocktailNameContainingIgnoreCaseAndVisibilityControlVisibilityControlType(String searchQuery, VisibilityControl.VisibilityControlType visibility);


} // end interface
