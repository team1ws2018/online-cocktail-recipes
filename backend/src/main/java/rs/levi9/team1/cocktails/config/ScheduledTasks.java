package rs.levi9.team1.cocktails.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import rs.levi9.team1.cocktails.domain.AppUser;
import rs.levi9.team1.cocktails.service.AppUserService;
import rs.levi9.team1.cocktails.service.EmailService;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.List;

@Component
public class ScheduledTasks {

    private AppUserService appUserService;
    private EmailService emailService;

    @Autowired
    public ScheduledTasks(AppUserService appUserService, EmailService emailService) {
        this.appUserService = appUserService;
        this.emailService = emailService;
    } // end constructor

    @Scheduled(cron = "0 0 12 * * ?")
    public void unblockAppUser() {
        Date currentDate = new Date();
        List<AppUser> appUsers = appUserService.findAllBlockedUsers();
        for (AppUser appUser : appUsers) {
            if (appUser.getBlockedUntil() != null) {
                if (appUser.getBlockedUntil().before(currentDate)) {
                    appUser.setBlocked(false);
                    appUser.setBlockedUntil(null);
                    appUserService.save(appUser);
                } // end inner if
            } // end outer if
        } // end for
    } // end unblockAppUser

    @Scheduled(fixedRate = 60000)
    public void testUnblockAppUser() {
        Date currentDate = new Date();
        List<AppUser> appUsers = appUserService.findAllBlockedUsers();
        for (AppUser appUser : appUsers) {
            if (appUser.getBlockedUntil() != null) {
                if (appUser.getBlockedUntil().before(currentDate)) {
                    appUser.setBlocked(false);
                    appUser.setBlockedUntil(null);
                    appUserService.save(appUser);
                    try {
                        emailService.sendEmail(appUser.getEmail(), "Account",
                                "Your account has been unblocked. \n\nOnline Cocktail Recipes");
                    } // end try
                    catch (MessagingException e) {
                        System.err.println(e);
                    } // end catch

                } // end inner if
            } // end outer if
        } // end for
    } // end testUnblockAppUser

} // end class
