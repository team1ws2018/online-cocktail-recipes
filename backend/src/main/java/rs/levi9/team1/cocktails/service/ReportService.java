package rs.levi9.team1.cocktails.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.levi9.team1.cocktails.domain.Report;
import rs.levi9.team1.cocktails.repository.ReportRepository;

import java.util.List;

@Transactional
@Service
public class ReportService {

    private ReportRepository reportRepository;

    @Autowired
    public ReportService(ReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    } // end constructor

    public List<Report> findAll() {
        return reportRepository.findAll();
    } // end findAll

    public Report findOne(Long id) {
        return reportRepository.findOne(id);
    } // end findOne

    public Report save(Report report) {
        return reportRepository.save(report);
    } // end save

    public void delete(Long id) {
        reportRepository.delete(id);
    } // end delete

    public Report findByReportedCommentId(Long commentId) {
        return reportRepository.findByReportedCommentId(commentId);
    }

} // end ReportService
