package rs.levi9.team1.cocktails.service;

import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Transactional
@Service
public class CaptchaService {

    private final String server = "https://www.google.com/recaptcha/api/siteverify";
    private final String privateCode = "6LfNTGkUAAAAAFdCVR1znmO-_ctF-JEjUoz6nunW";
    private RestTemplate rest;
    private HttpHeaders headers;
    private HttpStatus status;

    public CaptchaService() {
        this.rest = new RestTemplate();
        this.headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "/");
    }

    public String post(String captchaCode, String json) {
        HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);
        ResponseEntity<String> responseEntity = rest.exchange(server + "?secret="+ privateCode + "&response=" + captchaCode, HttpMethod.POST, requestEntity, String.class);
        this.setStatus(responseEntity.getStatusCode());
        return responseEntity.getBody();
    }

    public HttpStatus getStatus() {
        return status;
    }

    private void setStatus(HttpStatus status) {
        this.status = status;
    }
}
