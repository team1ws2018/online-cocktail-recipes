package rs.levi9.team1.cocktails.domain;

import javax.persistence.*;
import javax.validation.constraints.Future;
import java.util.*;

@Entity
public class AppUser extends BaseEntity {

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    private Boolean blocked;

//    @Future
    @Temporal(TemporalType.DATE)
    private Date blockedUntil;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable( joinColumns = @JoinColumn(name = "app_user_id"),
                inverseJoinColumns = @JoinColumn(name = "app_user_role_id") )
    private Set<Role> roles = new HashSet<>(Arrays.asList(new Role(2L, Role.RoleType.ROLE_USER)));

    private Boolean emailNotification = true;

    public AppUser() {

    } // end constructor

    public AppUser(String username, String email, String password, Boolean blocked, Date blockedUntil, Set<Role> roles, Boolean emailNotification) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.blocked = blocked;
        this.blockedUntil = blockedUntil;
        this.roles = roles;
        this.emailNotification = emailNotification;
    } // end constructor

    public String getUsername() {
        return username;
    } // end getter getUsername

    public void setUsername(String username) {
        this.username = username;
    } // end setter setUsername

    public String getEmail() {
        return email;
    } // end getter getEmail

    public void setEmail(String email) {
        this.email = email;
    } // end setter setEmail

    public String getPassword() {
        return password;
    } // end getter getPassword

    public void setPassword(String password) {
        this.password = password;
    } // end setter setPassword

    public Boolean getBlocked() {
        return blocked;
    } // end getter getBlocked

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    } // end setter setBlocked

    public Date getBlockedUntil() {
        return blockedUntil;
    }

    public void setBlockedUntil(Date blockedUntil) {
        this.blockedUntil = blockedUntil;
    }

    public Set<Role> getRoles() {
        return roles;
    } // end getter getRoles

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    } // end setter setRoles

    public Boolean getEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(Boolean emailNotification) {
        this.emailNotification = emailNotification;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        AppUser that = (AppUser) o;

        if (!username.equals(that.username))
            return false;
        if(!email.equals(that.email))
            return false;

        return password.equals(that.password);
    } // end equals

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    } // end hashCode

    @Override
    public String toString() {
        return "AppUser{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", blocked=" + blocked +
                ", blockedUntil=" + blockedUntil +
                ", roles=" + roles +
                ", emailNotification=" + emailNotification +
                '}';
    } // end toString

} // end class