package rs.levi9.team1.cocktails.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Report extends BaseEntity {

    private String reportedCommentText;

    private Long reportedCommentId;

    private Long cocktailId;

    private Long reportedUserId;

    private String reportedUsername;

    private Boolean resolved = false;

    public Report() {

    } // end constructor

    public Boolean getResolved() {
        return resolved;
    }

    public void setResolved(Boolean resolved) {
        this.resolved = resolved;
    }

    public String getReportedCommentText() {
        return reportedCommentText;
    }

    public void setReportedCommentText(String reportedCommentText) {
        this.reportedCommentText = reportedCommentText;
    }

    public Long getCocktailId() {
        return cocktailId;
    }

    public void setCocktailId(Long cocktailId) {
        this.cocktailId = cocktailId;
    }

    public Long getReportedUserId() {
        return reportedUserId;
    }

    public void setReportedUserId(Long reportedUserId) {
        this.reportedUserId = reportedUserId;
    }

    public String getReportedUsername() {
        return reportedUsername;
    }

    public void setReportedUsername(String reportedUsername) {
        this.reportedUsername = reportedUsername;
    }

    public Long getReportedCommentId() {
        return reportedCommentId;
    }

    public void setReportedCommentId(Long reportedCommentId) {
        this.reportedCommentId = reportedCommentId;
    }
} // end Report
