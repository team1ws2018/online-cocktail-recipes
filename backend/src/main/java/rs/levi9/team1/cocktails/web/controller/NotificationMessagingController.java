package rs.levi9.team1.cocktails.web.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
@CrossOrigin(origins = "http://localhost:4200")
public class NotificationMessagingController {

    @MessageMapping("/notification")
    @SendTo("/notification/recived")
    public String greeting() throws Exception {
        Thread.sleep(1000); // simulated delay
        return "Recived";
    }

}
