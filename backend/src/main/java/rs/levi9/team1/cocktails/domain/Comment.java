package rs.levi9.team1.cocktails.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@Entity
public class Comment extends BaseEntity {

    @NotNull
    @Type(type = "text")
    private String commentText;

    @ManyToOne(optional = false)
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;

    @NotNull
    private Long cocktailId;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentPostDate = new Date();

    public Comment() {

    } // end constructor

    public Comment(String commentText, AppUser appUser, Long cocktailId, Date commentPostDate) {
        this.commentText = commentText;
        this.appUser = appUser;
        this.cocktailId = cocktailId;
        this.commentPostDate = commentPostDate;
    } // end constructor

    public String getCommentText() {
        return commentText;
    } // end getter getCommentText

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    } // end setter setCommentText

    public AppUser getAppUser() {
        return appUser;
    } // end getter getAppUser

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    } // end setter setAppUser

    public Long getCocktailId() {
        return cocktailId;
    }

    public void setCocktailId(Long cocktailId) {
        this.cocktailId = cocktailId;
    }

    public Date getCommentPostDate() {
        return commentPostDate;
    } // end getter getCommentPostDate

    public void setCommentPostDate(Date commentPostDate) {
        this.commentPostDate = commentPostDate;
    } // end setter setCommentPostDate

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Comment comment = (Comment) o;
        return Objects.equals(commentText, comment.commentText) &&
                Objects.equals(commentPostDate, comment.commentPostDate);
    } // end equals

    @Override
    public int hashCode() {
        return Objects.hash(commentText, commentPostDate);
    } // end hashCode

    @Override
    public String toString() {
        return "Comment{" +
                "commentText='" + commentText +'\'' +
                ", commentPostDate=" + commentPostDate +
                '}';
    } // end toString

} // end class
