package rs.levi9.team1.cocktails.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.levi9.team1.cocktails.domain.Cocktail;
import rs.levi9.team1.cocktails.domain.VisibilityControl;
import rs.levi9.team1.cocktails.repository.CocktailRepository;

import java.util.List;

@Transactional
@Service
public class CocktailService {

    private CocktailRepository cocktailRepository;

    @Autowired
    public CocktailService(CocktailRepository cocktailRepository) {
        this.cocktailRepository = cocktailRepository;
    } // end constructor

    public List<Cocktail> findAll() {
        return cocktailRepository.findAll();
    } // end findAll

    public Cocktail findOne(Long id) {
        return cocktailRepository.findOne(id);
    } // end findOne

    public Cocktail save(Cocktail cocktail) {
        return cocktailRepository.save(cocktail);
    } // end save

    public Cocktail update(Cocktail cocktail) {
        return cocktailRepository.save(cocktail);
    }

    public void delete(Long id) {
        cocktailRepository.delete(id);
    } // end class

    public List<Cocktail> findAllByAppUserId(Long id) {
        return cocktailRepository.findAllByAppUserId(id);
    }

    public void deleteAll(List<Cocktail> cocktails) {
        cocktailRepository.delete(cocktails);
    }

    public Long countAllCocktails() {
        return cocktailRepository.count();
    }

    public Long countCocktailsFromUser(Long id) {
        return cocktailRepository.countByAppUserId(id);
    }

    public List<Cocktail> findCocktailsByCategory(String category) {
        return cocktailRepository.findAllByCocktailCategory(category);
    }

    public List<Cocktail> findCocktailsByType(String cocktailType) {
        return cocktailRepository.findAllByCocktailType(cocktailType);
    }

    public List<Cocktail> findCocktailsByGlassType(String glassType) {
        return cocktailRepository.findAllByGlassType(glassType);
    }

    public List<Cocktail> findCocktailsByName(String searchQuery) {
        return cocktailRepository.findAllByCocktailNameContainingIgnoreCase(searchQuery);
    }

    public List<Cocktail> findPublicCocktails() {
        return cocktailRepository.findAllByVisibilityControlVisibilityControlType(VisibilityControl.VisibilityControlType.PUBLIC);
    }

    public List<Cocktail> findPublicCocktailsByCategory(String category) {
        return cocktailRepository.findAllByCocktailCategoryAndVisibilityControlVisibilityControlType(category, VisibilityControl.VisibilityControlType.PUBLIC);
    }

    public List<Cocktail> findPublicCocktailsByType(String cocktailType) {
        return cocktailRepository.findAllByCocktailTypeAndVisibilityControlVisibilityControlType(cocktailType, VisibilityControl.VisibilityControlType.PUBLIC);
    }

    public List<Cocktail> findPublicCocktailsByGlassType(String glassType) {
        return cocktailRepository.findAllByGlassTypeAndVisibilityControlVisibilityControlType(glassType, VisibilityControl.VisibilityControlType.PUBLIC);
    }

    public List<Cocktail> findPublicCocktailsByName(String searchQuery) {
        return cocktailRepository.findAllByCocktailNameContainingIgnoreCaseAndVisibilityControlVisibilityControlType(searchQuery, VisibilityControl.VisibilityControlType.PUBLIC);
    }

} // end class
