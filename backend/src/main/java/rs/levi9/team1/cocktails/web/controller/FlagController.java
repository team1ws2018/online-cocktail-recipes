package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.team1.cocktails.domain.Flag;
import rs.levi9.team1.cocktails.service.FlagService;

import java.util.List;

@RestController
@RequestMapping("flags")
@CrossOrigin(origins = "http://localhost:4200")
public class FlagController {

    private FlagService flagService;

    @Autowired
    public FlagController(FlagService flagService) {
        this.flagService = flagService;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity saveFlag(@RequestBody Flag flag) {
      Flag flagToSave = flagService.save(flag);
      return new ResponseEntity(flagToSave, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity findAll() {
        List<Flag> flags = flagService.findAll();
        if (flags == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        return new ResponseEntity(flags, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "/user/{userId}/cocktail/{cocktailId}", method = RequestMethod.GET)
    public ResponseEntity findUserCocktailFlag(@PathVariable("userId") Long userId, @PathVariable("cocktailId") Long cocktailId) {
        Flag flag = flagService.findByAppUserIdAndCocktailId(userId, cocktailId);
        return new ResponseEntity(flag, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "tried/user/{userId}/cocktail/{cocktailId}", method = RequestMethod.GET)
    public ResponseEntity isUserTriedCocktail(@PathVariable("userId") Long userId, @PathVariable("cocktailId") Long cocktailId) {
        Flag flag = flagService.findByAppUserIdAndCocktailId(userId, cocktailId);
        if (flag == null) {
            return new ResponseEntity(Boolean.FALSE, HttpStatus.OK);
        }
        Boolean tried = flag.isTried();
        return new ResponseEntity(tried, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(path = "tried/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity findAllTriedFlags(@PathVariable("userId") Long userId) {
        List<Flag> triedCocktails = flagService.findAllTriedFlags(userId);
        if (triedCocktails == null) {
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
        else {
            return new ResponseEntity(triedCocktails, HttpStatus.OK);
        }
    } // end findAllTriedFlags
}
