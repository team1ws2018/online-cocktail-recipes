package rs.levi9.team1.cocktails.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cocktail extends BaseEntity {

    @NotNull
    private String cocktailName;

    @NotNull
    private String cocktailCategory;

    @NotNull
    private String cocktailType;

    private String cocktailScreenshotUrl;

    @NotNull
    private String glassType;

    @NotNull
    @Type(type = "text")
    private String instructions;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "visibility_control_id")
    private VisibilityControl visibilityControl = new VisibilityControl(1L, VisibilityControl.VisibilityControlType.PUBLIC);

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "app_user_id")
    private AppUser appUser;

    public Cocktail() {

    } // end constructor

    public Cocktail(String cocktailName, String cocktailCategory, String cocktailType, String cocktailScreenshotUrl, String glassType, String instructions, VisibilityControl visibilityControl, AppUser appUser) {
        this.cocktailName = cocktailName;
        this.cocktailCategory = cocktailCategory;
        this.cocktailType = cocktailType;
        this.cocktailScreenshotUrl = cocktailScreenshotUrl;
        this.glassType = glassType;
        this.instructions = instructions;
        this.visibilityControl = visibilityControl;
        this.appUser = appUser;
    }

    public String getCocktailName() {
        return cocktailName;
    }

    public void setCocktailName(String cocktailName) {
        this.cocktailName = cocktailName;
    }

    public String getCocktailCategory() {
        return cocktailCategory;
    }

    public void setCocktailCategory(String cocktailCategory) {
        this.cocktailCategory = cocktailCategory;
    }

    public String getCocktailType() {
        return cocktailType;
    }

    public void setCocktailType(String cocktailType) {
        this.cocktailType = cocktailType;
    }

    public String getCocktailScreenshotUrl() {
        return cocktailScreenshotUrl;
    }

    public void setCocktailScreenshotUrl(String cocktailScreenshotUrl) {
        this.cocktailScreenshotUrl = cocktailScreenshotUrl;
    }

    public String getGlassType() {
        return glassType;
    }

    public void setGlassType(String glassType) {
        this.glassType = glassType;
    }


    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public VisibilityControl getVisibilityControl() {
        return visibilityControl;
    }

    public void setVisibilityControl(VisibilityControl visibilityControl) {
        this.visibilityControl = visibilityControl;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

} // end class
