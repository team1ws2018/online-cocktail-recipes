package rs.levi9.team1.cocktails.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.levi9.team1.cocktails.domain.Rating;
import rs.levi9.team1.cocktails.repository.RatingRepository;

import java.util.List;

@Transactional
@Service
public class RatingService {

    private RatingRepository ratingRepository;

    @Autowired
    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    } // end constructor

    public List<Rating> findAll() {
        return ratingRepository.findAll();
    }

    public List<Rating> findAllByUserId(Long id) {
        return ratingRepository.findAllByAppUserId(id);
    }

    public Rating findOne(Long id) {
        return ratingRepository.findOne(id);
    } // end findOne

    public Rating findByAppUserIdAndCocktailId(Long uid, Long cid) {
        return ratingRepository.findByAppUserIdAndCocktailId(uid, cid);
    }

    public Rating save(Rating rating) {
        return ratingRepository.save(rating);
    } // end save

    public Rating update(Rating rating) {
        return ratingRepository.save(rating);
    } // end update

    public void delete(Long id) {
        ratingRepository.delete(id);
    }

    public void deleteAllRatingsFromUser(Long id) {
        ratingRepository.deleteAllByAppUserId(id);
    }

    public Double averageCocktailRating(Long id) {
        return ratingRepository.averageCocktailRating(id);
    }
}
