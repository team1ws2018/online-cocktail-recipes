package rs.levi9.team1.cocktails.domain;

public class CocktailsStatistic {

    private Long numberOfCocktails;

    public CocktailsStatistic(Long numberOfAllCockatils) {
        this.numberOfCocktails = numberOfAllCockatils;
    }

    public Long getNumberOfCocktails() {
        return numberOfCocktails;
    }

    public void setNumberOfCocktails(Long numberOfCocktails) {
        this.numberOfCocktails = numberOfCocktails;
    }
}
