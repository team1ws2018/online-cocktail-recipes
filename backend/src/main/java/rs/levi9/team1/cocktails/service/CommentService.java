package rs.levi9.team1.cocktails.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.levi9.team1.cocktails.domain.Comment;
import rs.levi9.team1.cocktails.repository.CommentRepository;

import java.util.List;

@Transactional
@Service
public class CommentService {

    private CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    } // end constructor

    public List<Comment> findAll() {
        return commentRepository.findAll();
    }  // end findAll

    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    } // end save

    public void delete(Long id) {
        commentRepository.delete(id);
    } // end delete

    public Comment findOne(Long id) {
        return commentRepository.findOne(id);
    } // end findOne

    public Comment update(Comment comment) {
        return commentRepository.save(comment);
    } // end update

    public List<Comment> findAllByCocktailId(Long id) {
        return commentRepository.findAllByCocktailIdOrderByCommentPostDateDesc(id);
    } // end findAllByCocktailId

    public void deleteAllCommentsFromUser(Long id) {
        commentRepository.deleteAllByAppUserId(id);
    } // end deleteAllCommentsFromUser

} // end class
