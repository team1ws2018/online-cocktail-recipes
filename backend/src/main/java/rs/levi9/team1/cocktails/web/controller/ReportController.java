package rs.levi9.team1.cocktails.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.team1.cocktails.domain.Report;
import rs.levi9.team1.cocktails.service.CommentService;
import rs.levi9.team1.cocktails.service.ReportService;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("reports")
@CrossOrigin(origins = "http://localhost:4200")
public class ReportController {

    private ReportService reportService;
    private CommentService commentService;

    @Autowired
    public ReportController(ReportService reportService, CommentService commentService) {
        this.reportService = reportService;
        this.commentService = commentService;
    } // end constructor

    @PreAuthorize("hasRole(ROLE_ADMIN")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity findAllReports() {
        List<Report> reports = this.reportService.findAll();
        if (Objects.isNull(reports))
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);

        return new ResponseEntity(reports, HttpStatus.OK);
    } // end findAllReports

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity save(@RequestBody Report report) {
        Report reportToSave = reportService.save(report);

        if (Objects.isNull(reportToSave))
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);

        return new ResponseEntity(reportToSave, HttpStatus.OK);
    } // end save

    @PreAuthorize("hasRole(ROLE_ADMIN")
    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        reportService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    } // end findAllReports

} // end ReportController
