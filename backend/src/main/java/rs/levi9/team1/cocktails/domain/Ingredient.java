package rs.levi9.team1.cocktails.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Ingredient extends BaseEntity {

    @NotNull
    private String ingredientName;

    private String ingredientMeasure;

    private String ingredientImageUrl;

    @ManyToOne
    @JoinColumn(name = "cocktail_id")
    private Cocktail cocktail;

    public Ingredient() {
    } // end constructor

    public Ingredient(String ingredientName, String ingredientMeasure, Cocktail cocktail) {
        this.ingredientName = ingredientName;
        this.ingredientMeasure = ingredientMeasure;
        this.cocktail = cocktail;
    }

    public String getIngredientName() {
        return ingredientName;
    } // end getter getIngredientNate

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    } // end setter setIngredientName

    public String getIngredientMeasure() {
        return ingredientMeasure;
    } // end getter getIngredientMeasure

    public void setIngredientMeasure(String ingredientMeasure) {
        this.ingredientMeasure = ingredientMeasure;
    } // end setter setIngredientMeasure

    public Cocktail getCocktail() {
        return cocktail;
    }

    public void setCocktail(Cocktail cocktail) {
        this.cocktail = cocktail;
    }

    public String getIngredientImageUrl() {
        return ingredientImageUrl;
    }

    public void setIngredientImageUrl(String ingredientImageUrl) {
        this.ingredientImageUrl = ingredientImageUrl;
    }

} // end class
