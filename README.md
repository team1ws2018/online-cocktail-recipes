# online-cocktail-recipes

Online Cocktail Recipes is a web application that allows users to store, share, track and review drinks. Users are allowed to create list of cocktails they want to try with own reviews and ratings, as well as interact with other users on site.

API used for cocktails https://www.thecocktaildb.com/



