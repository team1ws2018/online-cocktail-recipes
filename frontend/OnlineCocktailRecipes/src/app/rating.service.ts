import { Injectable } from '@angular/core';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { AuthService } from './login/auth-service.service';

@Injectable()
export class RatingService {

  API = 'http://localhost:8080/ratings/';

  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  saveRating(rating) {
    return this.httpClient.post(this.API, rating, {headers: this.authService.getAuthHeaders()})
  }

  getAverageCocktailRating(cocktailId) {
    return this.httpClient.get<number>(this.API + "cocktail/" + cocktailId);
  }

  getCocktailRatingFromUser(cocktailId: number, userId: number) {
    return this.httpClient.get(this.API + "cocktail/" + cocktailId+ "/user/" + userId, {headers: this.authService.getAuthHeaders()});
  }

  updateRating(rating) {
    return this.httpClient.put(this.API, rating, {headers: this.authService.getAuthHeaders()});
  }

}
