import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CocktailApiService } from '../coctail-api.service';
import { Router } from '../../../node_modules/@angular/router';
import { Subscription, Observable } from '../../../node_modules/rxjs';
import { CocktailConverterService } from '../util/cocktail-converter.service';
import { AuthService } from '../login/auth-service.service';
import { CocktailService } from '../cocktail.service';

@Component({
  selector: 'cocktail-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  CATEGORY = "category";
  TYPE = "type";
  GLASS = "glass";
  searchTerm: string = "";
  apiJson: any;
  cocktails = [];
  categoryCocktails$: Observable<any[]>;
  latestCocktails = [];
  categoryCocktails = [];
  recivedCategory = false;
  selectedCategory = "Cocktail";
  searchByCategoryName = ""

  typeCocktails = [];
  recivedType = false;
  selectedType = "Alcoholic";
  searchByTypeName = ""

  glassCocktails = [];
  recivedGlass = false;
  selectedGlass = "Cocktail glass";
  searchByGlassName = ""

  authenticated = false;
  auth$: Subscription;
  randomizer: Subscription;

  randomCocktail = {cocktailName: "", cocktailScreenshotUrl: "../assets/image_placeholder.png"};
  


  constructor(private cocktailApiService: CocktailApiService, private cocktailService: CocktailService, private router: Router, private cocktailMergeService: CocktailConverterService, public authService: AuthService) { }

  ngOnInit() {
    // this.auth$ = this.authService.userIsAuthenticated.take(1).subscribe(any => this.authenticated = true);
    this.cocktailService.findAllCocktails().subscribe(cocktails => {
      const temp: any[] = cocktails;
      this.latestCocktails = cocktails.reverse();
    });
    this.pickRandomCocktail();
    //pick random cocktail every 10 seconds
    this.randomizer = Observable.interval(10000).subscribe(val => this.pickRandomCocktail());
    this.populateByCategory(this.selectedCategory);
    this.populateByType(this.selectedType);
    this.populateByGlassType(this.selectedGlass);

  }

  ngOnDestroy() {
    this.randomizer.unsubscribe();
  }

  //Helper method populate cocktail by category
  private populateByCategory(category: string) {
    this.categoryCocktails = [];
    let tempCocktails = []
    this.recivedCategory = false;
    this.selectedCategory = category;
    let dataCocktailCategory = this.cocktailMergeService.getCocktailsByCategory(this.selectedCategory);
    dataCocktailCategory.apiCocktails.subscribe(cocktails =>{ cocktails.forEach(
      cocktail => {
        tempCocktails.push(cocktail);
      });
      dataCocktailCategory.cocktails.subscribe(
        cocktails => { cocktails.forEach(
          cocktail => {
            tempCocktails.push(cocktail);
          });
        });

        setTimeout(() => {
          this.recivedCategory = true;
          this.categoryCocktails = tempCocktails;
        }, 500);
        
    })
  }

  //Helper method populate cocktail by type
  private populateByType(type: string) {
    this.typeCocktails = [];
    let tempCocktails = []
    this.recivedType = false;
    this.selectedType = type;
    let dataCocktailType = this.cocktailMergeService.getCocktailsByCocktailType(this.selectedType);
    dataCocktailType.apiCocktails.subscribe(cocktails =>{ cocktails.forEach(
      cocktail => {
        tempCocktails.push(cocktail);
      });
      dataCocktailType.cocktails.subscribe(
        cocktails => { cocktails.forEach(
          cocktail => {
            tempCocktails.push(cocktail);
          });
        });

        setTimeout(() => {
          this.recivedType = true;
          this.typeCocktails = tempCocktails;
        }, 500);
        
    })
  }

  //Helper method populate cocktail by glass type
  private populateByGlassType(glass: string) {
    this.glassCocktails = [];
    let tempCocktails = []
    this.recivedGlass = false;
    this.selectedGlass = glass;
    let dataCocktailGlass = this.cocktailMergeService.getCocktailsByGlassType(this.selectedGlass);
    dataCocktailGlass.apiCocktails.subscribe(cocktails =>{ cocktails.forEach(
      cocktail => {
        tempCocktails.push(cocktail);
      });
      dataCocktailGlass.cocktails.subscribe(
        cocktails => { cocktails.forEach(
          cocktail => {
            tempCocktails.push(cocktail);
          });
        });

        setTimeout(() => {
          this.recivedGlass = true;
          this.glassCocktails = tempCocktails;
        }, 500);
        
    })
  }

  private pickRandomCocktail() {
    this.cocktailApiService.getRandomCocktail().subscribe(any => {
      const tempRandomCocktail = any;
      this.randomCocktail = tempRandomCocktail;
    });
   
  }

  goToCocktailDetails(idDrink: number){
    this.router.navigate(['/cocktail/' + idDrink])
  }

  goToCreateCocktailPage() {
    this.router.navigate(['/create'])
  }

  filterByCategory(event) {
    this.populateByCategory(event);
  }

  filterByType(event) {
    this.populateByType(event);
  }

  filterByGlass(event) {
    this.populateByGlassType(event);
  }

  searchByCategory(event) {
    this.searchByCategoryName = event;
  }

  searchByType(event) {
    this.searchByTypeName = event;
  }

  searchByGlass(event) {
    this.searchByGlassName = event;
  }

  searchCocktails(searchTerm: string) {
    this.searchTerm = searchTerm;
    this.cocktails = [];
    this.apiJson = null;
    this.cocktailApiService.searchCocktailByName(searchTerm).subscribe(any => {
      this.apiJson = any;
      // console.log(this.apiJson);
      if(this.apiJson.drinks) {
      this.apiJson.drinks.forEach(element => {
        this.cocktails.push(element);
      });
    }});
  }

  deletedCocktail() {
    this.cocktailService.findAllCocktails().subscribe(cocktails => {
      const temp: any[] = cocktails;
      this.latestCocktails = cocktails.reverse();
    });
  }

}
