import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomCocktailCardComponent } from './random-cocktail-card.component';

describe('RandomCocktailCardComponent', () => {
  let component: RandomCocktailCardComponent;
  let fixture: ComponentFixture<RandomCocktailCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RandomCocktailCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomCocktailCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
