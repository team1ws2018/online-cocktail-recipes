import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'cocktail-random-card',
  templateUrl: './random-cocktail-card.component.html',
  styleUrls: ['./random-cocktail-card.component.css']
})
export class RandomCocktailCardComponent implements OnInit {
  @Output() detailsCocktail = new EventEmitter<number>();

  @Input() randomCocktail: any;

  constructor() { }

  ngOnInit() {
  }

  emitDetailsCocktail(id: number) {
    this.detailsCocktail.emit(id);
  }

}
