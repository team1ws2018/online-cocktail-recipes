import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CocktailApiService } from '../../coctail-api.service';
import { RatingService } from '../../rating.service';
import { CocktailService } from '../../cocktail.service';
import { AuthService } from '../../login/auth-service.service';

@Component({
  selector: 'cocktail-list',
  templateUrl: './cocktail-list.component.html',
  styleUrls: ['./cocktail-list.component.css']
})
export class CocktailListComponent implements OnInit {
  @Output() detailsCocktail = new EventEmitter<number>();
  @Output() deletedCocktail = new EventEmitter();
  @Input() cocktail: any;

  starCount = 5;
  currentRate = 0;


  constructor(private cocktailApiService: CocktailApiService, private ratingService: RatingService, private cocktailService: CocktailService, public authService: AuthService) { }

  ngOnInit() {
    this.ratingService.getAverageCocktailRating(this.cocktail.id).subscribe(
      any => {
        if (any) {
          this.currentRate = any;
        }
      });
  }

  deleteCocktail(cocktailId: number) {
    this.cocktailService.deleteCocktail(cocktailId).subscribe(any => this.deletedCocktail.emit());
  }

  emitDetailsCocktail(id: number) {
    this.detailsCocktail.emit(id);
  }
}

