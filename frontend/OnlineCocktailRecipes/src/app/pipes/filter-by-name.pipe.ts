import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByName'
})
export class FilterByNamePipe implements PipeTransform {

  transform(cocktails: any[], name: string): any[] {
    if (name) {
      return cocktails.filter(cocktail => {
        if (cocktail.cocktailName.toLowerCase().indexOf(name.toLowerCase()) !== -1) {
          return true;
      } else {
        return false;
      }
      });
    }
    return cocktails;
  }

}
