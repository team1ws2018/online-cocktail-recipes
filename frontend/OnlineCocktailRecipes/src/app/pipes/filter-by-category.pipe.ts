import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByCategory'
})
export class FilterByCategoryPipe implements PipeTransform {

  transform(cocktails: any[], category: string): string[] {
    if (category) {
      return cocktails.filter(cocktail => cocktail.cocktailCategory === category);
    }
    return cocktails;
  }

}
