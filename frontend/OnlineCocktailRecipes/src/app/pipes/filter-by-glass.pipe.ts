import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByGlass'
})
export class FilterByGlassPipe implements PipeTransform {

  transform(cocktails: any[], glass: string): string[] {
    if (glass) {
      return cocktails.filter(cocktail => cocktail.glassType === glass);
    }
    return cocktails;
  }

}
