import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByType'
})
export class FilterByTypePipe implements PipeTransform {

transform(cocktails: any[], type: string): string[] {
  if (type) {
    return cocktails.filter(cocktail => cocktail.cocktailType === type);
  }
  return cocktails;
}

}
