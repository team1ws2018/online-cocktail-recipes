import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AuthService } from '../login/auth-service.service';
import { Router } from '../../../node_modules/@angular/router';
import { NotificationService } from '../notification.service';
import { Observable, Subscription } from '../../../node_modules/rxjs';

@Component({
  selector: 'cocktail-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit, OnDestroy {
  randomizer: Subscription;
  notifications$: Observable<any>;
  userAuthenticatedSubsrcription: Subscription
  currentUserId: number;
  constructor(public authService: AuthService, private router: Router, private notificationService: NotificationService) { }

  ngOnInit() {
    this.userAuthenticatedSubsrcription = this.authService.userIsAuthenticated.subscribe(auth => {
      if(auth.authenticated) {
      this.currentUserId = auth.id;
      this.notifications$ = this.notificationService.getUserNotifications(this.currentUserId);
      this.randomizer = Observable.interval(2000).subscribe(val => this.notifications$ = this.notificationService.getUserNotifications(this.currentUserId));
    } else { 
      this.randomizer.unsubscribe(); 
    }
    })
    
  
  }

  ngOnDestroy() {
    this.randomizer.unsubscribe();
  }

  navigateToUserProfile() {
    this.router.navigate(['user/'+ this.authService.currentUser.id]);
  }

}
