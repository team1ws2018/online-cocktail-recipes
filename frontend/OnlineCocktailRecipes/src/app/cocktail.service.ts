import { Injectable } from '@angular/core';
import { HttpClient, HttpXsrfTokenExtractor } from '../../node_modules/@angular/common/http';
import { AuthService } from './login/auth-service.service';
import { CocktailApiService } from './coctail-api.service';
import { Observable } from '../../node_modules/rxjs';

@Injectable()
export class CocktailService {
  API_COCKTAILS = "http://localhost:8080/cocktails/";
  API_INGREDIENTS = "http://localhost:8080/ingredients/";
  SEARCH = "search/";
  NAME = "name/"
  PUBLIC = "public/"
  CATEGORY = "category/";
  GLASS = "glass/";
  TYPE = "type/";


  constructor(private httpClient: HttpClient, private authService: AuthService, private cocktailApiService: CocktailApiService) { }

  findAllUserCocktails(userId: number) {
    return this.httpClient.get(this.API_COCKTAILS + "user/" + userId, {headers: this.authService.getAuthHeaders()});
  }

  getCocktailWithId(id: number): Observable<any> {
    return this.httpClient.get(this.API_COCKTAILS + id);
  }

  saveCocktail(cocktail: any) {
    return this.httpClient.post(this.API_COCKTAILS, cocktail, {headers: this.authService.getAuthHeaders()});
  }

  findAllCocktails(): Observable<any> {
    if (this.authService.isAuthenticated()) {
      return this.httpClient.get(this.API_COCKTAILS, {headers: this.authService.getAuthHeaders()});
    } else {
      return this.httpClient.get(this.API_COCKTAILS + this.SEARCH + this.PUBLIC);
    }
  }

  findCocktailsByCategory(category: string): Observable<any> {
    if (this.authService.isAuthenticated()) {
      return this.httpClient.get(this.API_COCKTAILS + this.SEARCH + this.CATEGORY + category, {headers: this.authService.getAuthHeaders()});
    } else {
      return this.httpClient.get(this.API_COCKTAILS + this.SEARCH + this.PUBLIC + this.CATEGORY + category);
    }
    
  }

  findCocktailsByType(cocktailType): Observable<any>  {
    if (this.authService.isAuthenticated()) {
      return this.httpClient.get(this.API_COCKTAILS + this.SEARCH + this.TYPE + cocktailType, {headers: this.authService.getAuthHeaders()});
    } else {
      return this.httpClient.get(this.API_COCKTAILS + this.SEARCH + this.PUBLIC + this.TYPE + cocktailType);
    }
  }

  findCocktailsByGlassType(glassType): Observable<any>  {
    if (this.authService.isAuthenticated()) {
      return this.httpClient.get(this.API_COCKTAILS + this.SEARCH + this.GLASS + glassType, {headers: this.authService.getAuthHeaders()});
    } else {
      return this.httpClient.get(this.API_COCKTAILS + this.SEARCH + this.PUBLIC +this.GLASS + glassType);
    }
  }

  findCocktailsByName(name): Observable<any>  {
    if (this.authService.isAuthenticated()) {
      return this.httpClient.get(this.API_COCKTAILS + this.SEARCH + this.NAME + name, {headers: this.authService.getAuthHeaders()});
    } else {
      return this.httpClient.get(this.API_COCKTAILS + this.SEARCH + this.PUBLIC +this.NAME + name);
    }
  }

  saveIngredient(ingredient: any) {
    return this.httpClient.post(this.API_INGREDIENTS, ingredient, {headers: this.authService.getAuthHeaders()});
  }

  getAllIngredientsFromCocktailId(cocktailId: number) {
    return this.httpClient.get(this.API_INGREDIENTS + "cocktail/" + cocktailId);
  }

  getCocktailsStatistic() {
    return this.httpClient.get(this.API_COCKTAILS + 'statistics', {headers: this.authService.getAuthHeaders()});
  }

  saveCocktailRating(cocktailId: number, rating: any) {
    return this.httpClient.post(this.API_COCKTAILS + cocktailId + '/rating', rating, {headers: this.authService.getAuthHeaders()})
  }

  getTriedCocktails(userId: number) {
    return this.httpClient.get('http://localhost:8080/flags/tried/user/' + userId, {headers: this.authService.getAuthHeaders()})
  }

  deleteCocktail(cocktailId: number){
    return this.httpClient.delete(this.API_COCKTAILS +  cocktailId,  {headers: this.authService.getAuthHeaders()})
  }
}
