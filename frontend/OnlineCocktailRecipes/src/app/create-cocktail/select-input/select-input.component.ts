import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cocktail-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select-input.component.css']
})
export class SelectInputComponent implements OnInit {

  @Input() options: string[];
  @Output() optionsSelected = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  selected(selectedOption: string) {
    this.optionsSelected.emit(selectedOption);
  }


}
