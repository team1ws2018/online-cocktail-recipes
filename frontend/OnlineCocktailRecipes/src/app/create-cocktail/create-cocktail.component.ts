import { Component, OnInit, AfterViewInit, ViewChild, Inject } from '@angular/core';
import { CocktailApiService } from '../coctail-api.service';
import { SelectInputComponent } from './select-input/select-input.component';
import { CocktailService } from '../cocktail.service';
import { AuthService } from '../login/auth-service.service';
import {Observable} from 'rxjs';
import {NgbTypeaheadConfig, NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import { CocktailDetailsComponent } from '../cocktail-details/cocktail-details.component';
import { MatTableDataSource, MatSlideToggleChange, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '../../../node_modules/@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'cocktail-dialog',
  template: ` 
          <h4 class="divider">       
           <span class="sans">{{data.cocktailName}}</span>
          </h4> 
          <img [src]="data.cocktailScreenshotUrl" style="width: 100%">
          <mat-dialog-content>   
          <h4>       
            Ingredients
          </h4> 
          <mat-list>
            <mat-list-item *ngFor="let ingredient of (data.ingredients)">{{ ingredient.ingredientName +" "+ ingredient.ingredientMeasure }}</mat-list-item>
          </mat-list>
          
          <strong>Category: </strong> {{data.cocktailCategory}} <br>
          <strong>Type: </strong> {{data.cocktailType}} <br>
          <strong>Glass: </strong> {{data.glassType}} <br>
          
          <strong>Instructions: </strong> {{data.instructions}}
          <br> <br>
          <strong><p>Would yot like to create your cocktail based on this one?</p></strong>
          </mat-dialog-content>
          <mat-dialog-actions> 
          <button mat-button mat-dialog-close tabindex="-1">No</button>
          <!-- The mat-dialog-close directive optionally accepts a value as a result for the dialog. -->
          <button mat-button tabindex="-1" (click)="onYesClick(data)">Yes</button>
          </mat-dialog-actions>  
          `,
  styleUrls: ['./create-cocktail.component.css']
})
export class CocktailDialog {
  constructor(public dialogRef: MatDialogRef<CocktailDialog>,
  @Inject(MAT_DIALOG_DATA) public data: any) {}

  // onNoClick(): void {
  //   this.dialogRef.close();
  // }

  onYesClick(data) :void {
    this.dialogRef.close(data);
  }

} // end CocktailDialog


@Component({
  selector: 'cocktail-create-cocktail',
  templateUrl: './create-cocktail.component.html',
  styleUrls: ['./create-cocktail.component.css'],
  providers: [NgbTypeaheadConfig] // add NgbTypeaheadConfig to the component providers
})

export class CreateCocktailComponent implements OnInit {
  @ViewChild(CocktailDetailsComponent) cocktailDetail: CocktailDetailsComponent;
  apiJson: any;
  cocktails = [];
  filterBy: {filterItem: string, filter: string};
  searchTerm: string = "";
  categoriesAPI: any;
  categories: string[] = [];
  alchocolicTypesAPI: any;
  alchocolicTypes:string[] = [];
  glassesAPI: any;
  glasses: string[] = [];

  ingredientNameForCocktail: string = '';

  ingredientName: string = '';
  ingredientAmount: string = '';
  ingredientMeasure: string = '';

  ingredients$: Observable<any>;
  ingredients: any[] = [];
  ingredientsList = [];

  cocktailScreenshotUrl:string;
  cocktailName: string;
  visibilityControlType: string = 'PUBLIC';
  selectedCategory: string;
  selectedType: string;
  selectedGlass: string;
  cocktailInstruction: string;

  ingredientsWithImages = [];

  modalIngredient: any;
  removable: boolean = true;
  visible: boolean = true;
  selectable: boolean = true;

  constructor(private modalService: NgbModal, private cocktailApiService: CocktailApiService, private cocktailService: CocktailService, 
    private authService: AuthService,public config: NgbTypeaheadConfig, public dialog: MatDialog, public snackBar: MatSnackBar, private router: Router) {
    // customize default values of typeaheads used by this component tree
    config.showHint = true;
   }

  ngOnInit() {
    //create list of ingredients
    this.cocktailApiService.getListOfIngredients().subscribe(any => {
      const apiObj: any = any;
      apiObj.drinks.forEach(ingredient => this.ingredientsList.push(ingredient.strIngredient1));
    })
    // console.log(this.ingredientsList);
    //create list of categories from API
    this.cocktailApiService.getListOfCategories().subscribe(any => { 
      this.categoriesAPI = any;
      this.categoriesAPI.drinks.forEach(category => this.categories.push(category.strCategory));
      
    });

     //create list of glass types from API
     this.cocktailApiService.getListOfGlasses().subscribe(any => { 
      this.glassesAPI = any;
      this.glassesAPI.drinks.forEach(glass => this.glasses.push(glass.strGlass));
    });

    //create list of coctail types (alchocolic non alchocolic...) from API
    this.cocktailApiService.getListOfAlchocolic().subscribe(any => { 
      this.alchocolicTypesAPI = any;
      this.alchocolicTypesAPI.drinks.forEach(alchocolType => {
        if(alchocolType.strAlcoholic) 
        this.alchocolicTypes.push(alchocolType.strAlcoholic)
      });
    });

    //initialised category, alcoholic and glass type
    // this.selectedCategory = "Cocktail";
    // this.selectedType = "Alcoholic";
    // this.selectedGlass = "Cocktail glass";

    this.ingredientsWithImages = this.cocktailApiService.getIngredientsListWithImages();
  } // end onInit

  // MODAL 
  openDialog(id): void {
    let dialogRef: MatDialogRef<CocktailDialog>;
    this.cocktailApiService.getCocktailDetail(id).subscribe(
      (cocktail) => {
      const cocktailApi = cocktail;
      dialogRef = this.dialog.open(CocktailDialog, {
        width: '500px',
        height: '700px',
        data: cocktailApi      
      });

      dialogRef.afterClosed().subscribe(
        result => {
          if (result === null || result === undefined || Object.values(result).length < 1) {
            // this.ingredients = [];
          } else {
            if (this.ingredients.length < 1) {
              this.ingredients = result.ingredients
              this.ingredients.forEach( ingredient =>
              ingredient.representation = ingredient.ingredientName + ' ' + ingredient.ingredientMeasure)
            
            this.cocktailName = result.cocktailName;
            this.cocktailScreenshotUrl = result.cocktailScreenshotUrl;
            this.selectedCategory = result.cocktailCategory;
            this.selectedType = result.cocktailType;
            this.selectedGlass = result.glassType;
            this.cocktailInstruction = result.instructions;
          } 
          else {
            this.ingredients = [];
            this.ingredients = result.ingredients;
              this.ingredients.forEach( ingredient =>
              ingredient.representation = ingredient.ingredientName + ' ' + ingredient.ingredientMeasure)
            
              this.cocktailName = result.cocktailName;
              this.cocktailScreenshotUrl = result.cocktailScreenshotUrl;
              this.selectedCategory = result.cocktailCategory;
              this.selectedType = result.cocktailType;
              this.selectedGlass = result.glassType;
              this.cocktailInstruction = result.instructions;
          }
        }
      
      });  
    });  
  } // end opetDialog

  findIngredients(cocktail) {
    for (let index = 1; index < 16; index++) { 
      if (cocktail["strIngredient" + index] === "" || cocktail["strIngredient" + index] === null) {
      
      } else {
      let ingredient = { 
        ingredientName: cocktail["strIngredient" + index],
        ingredientMeasure: "",
        strThumb: this.cocktailApiService.getIngredientImageSmall["strIngredient" + index]
      }

      if (cocktail['strMeasure' + index] === "" ||  cocktail['strMeasure' + index] === null) {
      
      } else {
        ingredient.ingredientMeasure = cocktail['strMeasure' + index];
      }
      this.ingredients.push(ingredient);
    }
    }
    // console.log(this.ingredients);
    return this.ingredients;
  }

  //search ingredients
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.ingredientsList.filter(v => v.toLowerCase().startsWith(term.toLocaleLowerCase())).splice(0, 10))
    );

    searchCocktailsByIngredients = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.ingredientsList.filter(v => v.toLowerCase().startsWith(term.toLocaleLowerCase())).splice(0, 10))
    );

  filterCocktailList(filter: {filterItem: string, filter: string}) {
    if(this.searchTerm === "") {
      this.getFilterByEmptySearchTerm(filter).subscribe(any => {
        this.apiJson = any;
        this.cocktails = this.apiJson.drinks;
      })
    } else {
      this.cocktails = this.getFilterBySearchTerm(filter);
    }
  }

  searchCocktails(searchTerm: string) {
    this.searchTerm = searchTerm;
    this.cocktails = [];
    this.cocktailApiService.searchCocktailsByIngredient(searchTerm).subscribe(any => {
      const temp = any
      if(temp) {
      temp.forEach(element => {
        this.cocktails.push(element);
      });
      
    }});
   
  }

  private getFilterByEmptySearchTerm(filterBy: {filterItem: string, filter: string}) {
    switch (filterBy.filterItem) {
      case "Category":
        return this.cocktailApiService.filterCocktailsByCategory(filterBy.filter);
        case "Glass":
        return this.cocktailApiService.filterCocktailsByGlass(filterBy.filter);
        case "Alcoholic":
        return this.cocktailApiService.filterCocktailsByAlchocolic(filterBy.filter);
     
    }
  }

  private getFilterBySearchTerm(filterBy: {filterItem: string, filter: string}) {
    switch (filterBy.filterItem) {
      case "Category":
        return this.cocktails.filter(cocktail => cocktail.strCategory === filterBy.filter);
        case "Glass":
        return this.cocktails.filter(cocktail => cocktail.strGlass === filterBy.filter);
        case "Alcoholic":
        return this.cocktails.filter(cocktail => cocktail.strAlcoholic === filterBy.filter);
     
    }
  }

  onSelectedCategory(selected: string) {
    this.selectedCategory = selected;
  }

  onSelectedType(selected: string) {
    this.selectedType = selected;
  }

  onSelectedGlass(selected: string) {
    this.selectedGlass = selected;
  }

  isIngredientNameValid() {
    if (this.ingredientName === null || this.ingredientName === '') {
      return false;
    } else return true;
  }

  isIngredientNameWhitespace() {    
    if (this.ingredientName.trim() === '') {
      return true;
    }
    else return false;
  }

  addIngredient() {
    if (this.ingredientName != null) {
      if (this.isIngredientNameWhitespace() === false) {
        if (this.ingredientAmount === null) {
          this.ingredientAmount = '';
        }
        if (this.ingredientMeasure === null) {
          this.ingredientMeasure = '';
          this.ingredientMeasure = this.ingredientMeasure.trim();
        }

    this.ingredients.unshift(
      {
        ingredientName: this.ingredientName,
        ingredientMeasure: this.ingredientAmount.toString() + " " + this.ingredientMeasure,
        ingredientImageUrl: this.cocktailApiService.getIngredientImageSmall(this.ingredientName),
        representation: this.ingredientName + " " + this.ingredientAmount.toString() + " " + this.ingredientMeasure,
        cocktail: {}
      }
    );
  }
    // console.log(this.ingredients);
    this.ingredientName = null;
    this.ingredientAmount = null
    this.ingredientMeasure = null;    
  }
}

  deleteIngredient(index: number) {
    // console.log(index);
    this.ingredients.splice(index, 1);
  }

  saveCocktail() {
    if (this.authService.isAuthenticated() ) {
      this.cocktailService.saveCocktail({
        cocktailName: this.cocktailName,
        visibilityControl :{
          id: this.visibilityControlType === 'PUBLIC' ? 1 : 2,
          visibilityControlType: this.visibilityControlType
        },
        cocktailCategory: this.selectedCategory,
        cocktailType: this.selectedType,
        glassType: this.selectedGlass,
        cocktailScreenshotUrl: this.cocktailScreenshotUrl === null ? '../assets/image_placeholder.png' : this.cocktailScreenshotUrl,
        instructions: this.cocktailInstruction,
        appUser: this.authService.getCurrentClientUser()
      }).subscribe(
        cocktail => {
          this.ingredients
                    .forEach(ingredient => { 
                      ingredient.cocktail = cocktail;
                      this.cocktailService.saveIngredient(ingredient).subscribe()
                    });
                    const msg = "Cocktail is saved";
                    this.openSnackBar(msg, 'OK');     
                    setTimeout(() => {
                      this.router.navigate(['/myrecipes']);
                  }, 1000);               
        });
        
    } else {
      this.router.navigate(['/login']);
    }
  }

  clearForm() {
    this.cocktailScreenshotUrl = '';
    this.cocktailName = '';
    this.cocktailInstruction = '';
    this.selectedCategory = '';
    this.selectedType = '';
    this.selectedGlass = '';
    this.ingredients = [];
    this.ingredientName = '';
    this.ingredientAmount = '';
    this.ingredientMeasure = '';
  }

  //show message
  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }

  ingredientClicked(ingredient: any) {
    // console.log(ingredient);
    this.modalIngredient = ingredient;
  }

  openCocktailDetailModal(content, id:number) {
    // console.log(id);
    this.modalService.open(content,{ centered: true });
  }

  onStatusChange(status: MatSlideToggleChange) {
    // console.log(status);
          if (status.checked) {
            this.visibilityControlType="PUBLIC";
          } else {
            this.visibilityControlType='RESTRICTED';
          }

    }
  
  }
