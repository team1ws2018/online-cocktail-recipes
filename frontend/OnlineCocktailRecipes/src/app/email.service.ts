import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '../../node_modules/@angular/common/http';
import { AuthService } from './login/auth-service.service';
import { User } from './users/user.model';

@Injectable()
export class EmailService {

  API = 'http://localhost:8080/email/';

  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  sendEmail(user: User) {
    return this.httpClient.post(this.API, user, {headers: this.authService.getAuthHeaders()});
  }
  sendForgottenEmail(email: string, headers: HttpHeaders) {
    return this.httpClient.post(this.API + "recovery", email, {headers: headers});
  }
}
