import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { CocktailApiService } from '../coctail-api.service';

@Component({
  selector: 'cocktail-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Output() cocktailFilterBy = new EventEmitter<string>();
  @Output() searchCocktails = new EventEmitter<string>();
  @Input() selectOption: string;

  optionsList = [];

  constructor(private cocktailApiService: CocktailApiService) { }

  ngOnInit() {
    switch (this.selectOption) {
      case "category":
      this.cocktailApiService.getListOfCategories().subscribe(
        any => {
          const tempList: any = any;
          tempList.drinks.forEach(element => {
            this.optionsList.push(element.strCategory);
          });
        });
        break;
      case "type":
      this.cocktailApiService.getListOfAlchocolic().subscribe(
        any => {
          const tempList: any = any;
          tempList.drinks.forEach(element => {
          this.optionsList.push(element.strAlcoholic);
          });
        });
        break;
      case "glass":
      this.cocktailApiService.getListOfGlasses().subscribe(
        any => 
        {
          const tempList: any = any;
          tempList.drinks.forEach(element => {
            this.optionsList.push(element.strGlass);
          });
        });
        break;
    } //end switch

}

  emitSelectedFilter(event) {
    this.cocktailFilterBy.emit(event.value);

  }

  emitSearchCocktails(searchTerm: string) {
    this.searchCocktails.emit(searchTerm);
  }

 

}
