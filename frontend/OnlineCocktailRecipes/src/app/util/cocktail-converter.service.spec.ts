import { TestBed, inject } from '@angular/core/testing';

import { CocktailConverterService } from './cocktail-converter.service';

describe('CocktailConverterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CocktailConverterService]
    });
  });

  it('should be created', inject([CocktailConverterService], (service: CocktailConverterService) => {
    expect(service).toBeTruthy();
  }));
});
