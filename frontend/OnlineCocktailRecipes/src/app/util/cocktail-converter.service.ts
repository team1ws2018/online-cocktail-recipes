import { Injectable } from '@angular/core';
import { CocktailApiService } from '../coctail-api.service';
import { CocktailService } from '../cocktail.service';
import { Observable } from '../../../node_modules/rxjs';

@Injectable()
export class CocktailConverterService {

  constructor(private cocktailApiService: CocktailApiService, private cocktailService: CocktailService) {}

  //Get cocktail detail by first checking in API cocktail, then if not found check coktail database returns object with two observable
  getCocktailDetail(cocktailId: number) {
        if(cocktailId > 1000) {
          return {
            cocktail: this.cocktailApiService.getCocktailDetail(cocktailId),
            ingredients: null
          };
        } else {
          return {
                  cocktail: this.cocktailService.getCocktailWithId(cocktailId),
                  ingredients: this.cocktailService.getAllIngredientsFromCocktailId(cocktailId)
                };
        }
  }

  //
  getCocktailsByCategory(category: string) {
    return {
      apiCocktails: this.cocktailApiService.filterCocktailsByCategory(category),
      cocktails: this.cocktailService.findCocktailsByCategory(category)
    }
  }

  getCocktailsByName(name: string) {
    return {
      apiCocktails: this.cocktailApiService.searchCocktailByName(name),
      cocktails: this.cocktailService.findCocktailsByName(name)
    }
  }

  getCocktailsByCocktailType(cocktailType: string) {
    return {
      apiCocktails: this.cocktailApiService.filterCocktailsByAlchocolic(cocktailType),
      cocktails: this.cocktailService.findCocktailsByType(cocktailType)
    }
  }

  getCocktailsByGlassType(glass: string) {
    return {
      apiCocktails: this.cocktailApiService.filterCocktailsByGlass(glass),
      cocktails: this.cocktailService.findCocktailsByGlassType(glass)
    }
  }

}
