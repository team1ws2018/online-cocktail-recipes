import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { RandomCocktailCardComponent } from './home/random-cocktail-card/random-cocktail-card.component';
import { HomeUserCocktailCardComponent } from './cards/home-user-cocktail-card/home-user-cocktail-card.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { AuthService } from './login/auth-service.service';
import { AuthGuard } from './auth-guard.service';
import { FormsModule } from '../../node_modules/@angular/forms';
import { AuthGuardAccessDeniedService } from './auth-guard-access-denied.service';
import { CocktailApiService } from './coctail-api.service';
import { SearchComponent } from './search/search.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ConfirmEqualValidatorDirective } from './sign-up/confirm-equal-validator.directive';
import { CreateCocktailComponent, CocktailDialog } from './create-cocktail/create-cocktail.component';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { ForgottenCredentialsComponent } from './forgotten-credentials/forgotten-credentials.component';
import { EmailService } from './email.service';
import { UsersService } from './users/users.service';
import { SelectInputComponent } from './create-cocktail/select-input/select-input.component';
import { IngredientCardComponent } from './cards/ingredient-card/ingredient-card.component';
import { CocktailService } from './cocktail.service';
import { IngredientDetailComponent } from './ingredient-detail/ingredient-detail.component';
import { NgbModule } from '../../node_modules/@ng-bootstrap/ng-bootstrap';
import { CocktailDetailsComponent, IngredientDialog } from './cocktail-details/cocktail-details.component';
import { MaterialModule } from './material/material.module';
import { AdminDashboardComponent, DialogDeleteUsers, DialogBlockUntil } from './admin-dashboard/admin-dashboard.component';
import { CocktailListComponent } from './home/cocktail-list/cocktail-list.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';
import { StarRaitingComponent } from './cocktail-details/star-raiting/star-raiting.component';
import { AdminReportsListComponent } from './admin-dashboard/admin-reports-list/admin-reports-list.component';
import { CommentsComponent } from './cocktail-details/comments/comments.component';
import { CommentListComponent } from './cocktail-details/comments/comment-list/comment-list.component';
import { CommentsService } from './cocktail-details/comments/comments.service';
import { ReportsService } from './reports.service';
import { MyRecipesComponent } from './my-recipes/my-recipes.component';
import { FavoritesComponent } from './my-recipes/favorites/favorites.component';
import { RatingService } from './rating.service';
import { NotificationComponent } from './notification/notification.component';
import { NotificationService } from './notification.service';
import { CocktailConverterService } from './util/cocktail-converter.service';
import { AuthGuardUserBlockedService } from './auth-guard-user-blocked.service'
import { FilterByCategoryPipe } from './pipes/filter-by-category.pipe';
import { FilterByTypePipe } from './pipes/filter-by-type.pipe';
import { FilterByGlassPipe } from './pipes/filter-by-glass.pipe';
import { FilterByNamePipe } from './pipes/filter-by-name.pipe';
import { JwSocialButtonsModule } from 'jw-angular-social-buttons';
import { FlagService } from './flag.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    RandomCocktailCardComponent,
    HomeUserCocktailCardComponent,
    LoginComponent,
    UsersComponent,
    SearchComponent,
    SignUpComponent,
    ConfirmEqualValidatorDirective,
    CreateCocktailComponent,
    ForgottenCredentialsComponent,
    SelectInputComponent,
    IngredientCardComponent,
    IngredientDetailComponent,
    CocktailDetailsComponent,
    CocktailListComponent,
    AdminDashboardComponent,
    DialogDeleteUsers,
    CocktailDialog,
    DialogBlockUntil,
    UserProfileComponent,
    StarRaitingComponent,
    AdminReportsListComponent,
    CommentsComponent,
    CommentListComponent,
    MyRecipesComponent,
    FavoritesComponent,
    NotificationComponent,
    FilterByCategoryPipe,
    FilterByTypePipe,
    FilterByGlassPipe,
    FilterByNamePipe,
    IngredientDialog
    
  ],
  entryComponents: [
    DialogDeleteUsers,
    CocktailDialog,
    DialogBlockUntil,
    IngredientDialog
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RecaptchaModule.forRoot(),
    RecaptchaFormsModule,
    NgbModule.forRoot(),
    MaterialModule,
    JwSocialButtonsModule 
  ],
  providers: [
    //add services
    AuthService,
    AuthGuard,
    AuthGuardAccessDeniedService,
    CocktailApiService,
    EmailService,
    UsersService,
    CocktailService,
    CommentsService,
    ReportsService,
    RatingService,
    NotificationService,
    CocktailConverterService,
    AuthGuardUserBlockedService,
    FlagService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
