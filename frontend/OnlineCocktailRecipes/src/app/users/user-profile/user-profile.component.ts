import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router
} from '../../../../node_modules/@angular/router';
import {
  UsersService
} from '../users.service';
import {
  AppUser
} from '../../admin-dashboard/admin-dashboard.component';
import {
  NgForm
} from '../../../../node_modules/@angular/forms';
import {
  MatSnackBar
} from '../../../../node_modules/@angular/material';

@Component({
  selector: 'cocktail-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  userId: number;
  username: string = "";
  email: string = "";
  emailNotification = true;
  appUser: AppUser;
  newPassword = "";

  constructor(private router: Router, private route: ActivatedRoute, private usersService: UsersService, public snackBar: MatSnackBar) {
    this.route.params.subscribe(params => this.userId = params['id']);
  }

  ngOnInit() {
    this.usersService.getUserWithId(this.userId).subscribe(user => {
      this.appUser = user;
      this.username = user.username;
      this.email = user.email;
      this.emailNotification = user.emailNotification;
    })
  }

  updateUser(ngForm: NgForm) {
    if (ngForm.value.password !== "") {
      this.appUser.password = ngForm.value.password;
    }
    this.appUser.username = this.username;
    this.appUser.email = this.email;
    this.appUser.emailNotification = this.emailNotification;
    this.usersService.updateUser(this.appUser).subscribe(any => this.openSnackBar("Updated", "OK"));
  }

  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
}
