export class User {
    public id: number;
    public username: string;
    public password: string;
    public email: string;
    public blocked: boolean;
    
    
    constructor(username: string, password: string, email: string, blocked?: boolean, id?: number){
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.blocked = blocked;
    }
}
