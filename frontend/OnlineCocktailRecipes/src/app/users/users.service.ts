import { Injectable } from '@angular/core';
import { User } from './user.model';
import { HttpClient, HttpHeaders } from '../../../node_modules/@angular/common/http';
import { AuthService } from '../login/auth-service.service';
import { Observable } from '../../../node_modules/rxjs';
import { AppUser } from '../admin-dashboard/admin-dashboard.component';

@Injectable()
export class UsersService {

  API = 'http://localhost:8080/users/';

  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  saveUser(user: User): Observable<any> {
    if (user.id) {
      return this.httpClient.put(this.API, user, { headers: this.authService.getAuthHeaders() });
  } else {
      return  this.httpClient.post(this.API, user, { headers: this.authService.getAuthHeaders() });
  }
  }
  registerUser(user: User, headers: HttpHeaders) {
    return this.httpClient.post(this.API, user, {headers});
  }

  getAllUser():Observable<AppUser[]> {
    return this.httpClient.get<AppUser[]>(this.API, { headers: this.authService.getAuthHeaders() });
  }

  updateUser(user: AppUser) {
    return this.httpClient.put<AppUser>(this.API, user, { headers: this.authService.getAuthHeaders()})
  } 

  getUserWithId(id: number) {
    return this.httpClient.get<AppUser>(this.API + id, { headers: this.authService.getAuthHeaders()});
  }

  deleteAllUsers(users: AppUser[]) {
    return this.httpClient.post(this.API+'delete/all', users, { headers: this.authService.getAuthHeaders()});
  }

  getUsersStatistics() {
    return this.httpClient.get(this.API + 'statistics', { headers: this.authService.getAuthHeaders()})
  }
}
