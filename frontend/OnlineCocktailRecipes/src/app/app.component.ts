import { Component, OnInit } from '@angular/core';
import { CocktailApiService } from './coctail-api.service';

@Component({
  selector: 'cocktail-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit  {

  constructor(private coctailApiService: CocktailApiService) {}

  ngOnInit() {
    this.coctailApiService.setIngredientsWithImages();
  }
}
