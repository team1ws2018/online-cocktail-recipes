import { Component, OnInit } from '@angular/core';
import { HttpHeaders } from '../../../node_modules/@angular/common/http';
import { EmailService } from '../email.service';
import { Router } from '../../../node_modules/@angular/router';
import { NgForm } from '../../../node_modules/@angular/forms';

@Component({
  selector: 'cocktail-forgotten-credentials',
  templateUrl: './forgotten-credentials.component.html',
  styleUrls: ['./forgotten-credentials.component.css']
})
export class ForgottenCredentialsComponent implements OnInit {

  base64Credential = btoa("admin" + ':' + "admin");
  headers = new HttpHeaders({
      authorization: 'Basic ' + this.base64Credential
    });
   
    error: Error;
    
  constructor(private emailService: EmailService, private router: Router) { }

  ngOnInit() {
  }

  onLogin(form: NgForm, headers: HttpHeaders) {
    const email = form.value.email;
    this.emailService.sendForgottenEmail(email, this.headers).subscribe(
      any  => {
      
        this.router.navigate(["/login"])
   
      },
    error => this.error = error);
}
}
