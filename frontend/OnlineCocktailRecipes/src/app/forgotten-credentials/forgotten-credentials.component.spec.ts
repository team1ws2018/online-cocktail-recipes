import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgottenCredentialsComponent } from './forgotten-credentials.component';

describe('ForgottenCredentialsComponent', () => {
  let component: ForgottenCredentialsComponent;
  let fixture: ComponentFixture<ForgottenCredentialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgottenCredentialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgottenCredentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
