import { Component, OnInit } from '@angular/core';
import { CocktailService } from '../../cocktail.service';
import { AuthService } from '../../login/auth-service.service';
import { CocktailConverterService } from '../../util/cocktail-converter.service';
import { Router } from '@angular/router';

@Component({
  selector: 'cocktail-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {

  triedCocktails: any[] = [];

  constructor(private router: Router, private cocktailService: CocktailService, private authService: AuthService, 
    private cocktailConvertService: CocktailConverterService) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.cocktailService.getTriedCocktails(this.authService.getCurrentClientUser().id).subscribe(
        flags => {
        const temp:any = flags;
        temp.forEach(flag => {
          const data = this.cocktailConvertService.getCocktailDetail(flag.cocktailId);
          data.cocktail.subscribe(
          cocktail => {                                 
            this.triedCocktails.push(cocktail);            
            });
        });
    });

  }
  }

  goToCocktailDetails(idDrink: number){
    this.router.navigate(['/cocktail/' + idDrink])
  }
}
