import { Component, OnInit } from '@angular/core';
import { CocktailService } from '../cocktail.service';
import { Router } from '../../../node_modules/@angular/router';
import { AuthService } from '../login/auth-service.service';
import { Observable } from '../../../node_modules/rxjs';
import { RatingService } from '../rating.service';

@Component({
  selector: 'cocktail-my-recipes',
  templateUrl: './my-recipes.component.html',
  styleUrls: ['./my-recipes.component.css']
})
export class MyRecipesComponent implements OnInit {

  userCocktails$: Observable<any>;
  userId: number;

  constructor(private cocktailService: CocktailService, public authService: AuthService, public router: Router, private ratingService: RatingService) { }

  ngOnInit() {
    this.userCocktails$ = this.cocktailService.findAllUserCocktails(this.authService.getCurrentClientUser().id);
  }

  goToCocktailDetails(idDrink: number){
    this.router.navigate(['/cocktail/' + idDrink])
  }

  deletedCocktail() {
    this.userCocktails$ = this.cocktailService.findAllUserCocktails(this.authService.getCurrentClientUser().id);
  }
}
