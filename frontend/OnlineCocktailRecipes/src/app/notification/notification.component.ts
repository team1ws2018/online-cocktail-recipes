import { Component, OnInit, Input } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'cocktail-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  @Input() notification: any;
  
  constructor(public router: Router, public notificationService: NotificationService) { }

  ngOnInit() {
  }

  goToNotificationCocktail(id: number){
    this.notificationService.deleteNotification(this.notification.id).subscribe(any => this.router.navigate(['/cocktail/' + id]))
  }

}
