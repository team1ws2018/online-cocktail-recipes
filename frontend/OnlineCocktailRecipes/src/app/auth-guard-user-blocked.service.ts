import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './login/auth-service.service';

@Injectable()
export class AuthGuardUserBlockedService {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const expectedRole = route.data.expectedRole;

    if (this.authService.isAuthenticated()) {
        if ( expectedRole === 'user' && this.authService.getCurrentClientUser().blocked === false)  {
            return true;
        }
        this.router.navigate(['/home']);
        return false;

    }
    this.router.navigate(['/login']);
    return false;
}

}
