import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { MatSnackBar } from '../../../../node_modules/@angular/material';
import { AuthService } from '../../login/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'cocktail-star-raiting',
  templateUrl: './star-raiting.component.html',
  styleUrls: ['./star-raiting.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class StarRaitingComponent implements OnInit {
  @Input('rating') private rating: number;
  @Input('starCount') private starCount: number;
  @Input('color') private color: string;
  @Output() private ratingUpdated = new EventEmitter();

  private snackBarDuration: number = 2000;
  private ratingArr = [];

  constructor(private snackBar: MatSnackBar, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    for (let index = 0; index < this.starCount; index++) {
      this.ratingArr.push(index);
    }
  }

  onClick(rating:number) {
    console.log(rating)
    if (this.authService.isAuthenticated()) {
      this.snackBar.open('You rated ' + rating + ' / ' + this.starCount, '', {
      duration: this.snackBarDuration
    });

    this.ratingUpdated.emit(rating);
    }
    else {
      this.router.navigate(['/login']);
    }
  }

  showIcon(index:number) {
    if (this.rating >= index + 1) {
      return 'star';
    } else {
      return 'star_border';
    }
  }
}

export enum StarRatingColor {
  primary = "primary",
  accent = "accent",
  warn = "warn"
}


