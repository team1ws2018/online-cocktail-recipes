import { Component, OnInit, Input, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { CocktailApiService } from '../coctail-api.service';
import { CocktailService } from '../cocktail.service';
import { AuthService } from '../login/auth-service.service';
import { RatingService } from '../rating.service';
import { NotificationService } from '../notification.service';
import { CocktailConverterService } from '../util/cocktail-converter.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '../../../node_modules/@angular/material';
import { FlagService } from '../flag.service';

@Component({
  selector: 'ingredient-dialog',
  template: `
          <h4 class="divider">       
            <span class="sans">{{data.ingredient.strIngredient}}</span>
          </h4>
          <img [src]="data.img" style="width: 100%"> 
          <mat-dialog-content>
          <p>{{data.ingredient.strDescription}}</p> 
          </mat-dialog-content>
          <mat-dialog-actions>
          <button mat-button mat-dialog-close tabindex="-1">Close</button>
          </mat-dialog-actions>        
  `,
  styleUrls: ['../create-cocktail/create-cocktail.component.css']
})
export class IngredientDialog {
  constructor(public dialogRef: MatDialogRef<IngredientDialog>,
  @Inject(MAT_DIALOG_DATA) public data: any) {}

} // end CocktailDialog


@Component({
  selector: 'cocktail-cocktail-details',
  templateUrl: './cocktail-details.component.html',
  styleUrls: ['./cocktail-details.component.css']
})

export class CocktailDetailsComponent implements OnInit {

cocktailId: number;
currentCocktail: any;
currentCocktailIngredients: any[];
recieved = false;
rating: number;
userRating: any;
starCount = 5;
starColor = 'accent';
currentRate = 0;
isTried = false;

  constructor(private flagService: FlagService, private cocktailMergeService: CocktailConverterService,public dialog: MatDialog, private router: Router, private route: ActivatedRoute, private cocktailApiService: CocktailApiService, private cocktailService: CocktailService, private authService: AuthService, private ratingService: RatingService, private notificationService: NotificationService) {
    this.route.params.subscribe(params => this.cocktailId = params['id']);
   }

  ngOnInit() {
    //get cocktail details
    const data = this.cocktailMergeService.getCocktailDetail(this.cocktailId);
    data.cocktail.subscribe(
      cocktail => {
        if(data.ingredients === null) {
          this.currentCocktail = cocktail;
          this.currentCocktailIngredients = cocktail.ingredients;
        } else {
          this.currentCocktail = cocktail;
          data.ingredients.subscribe(
            ingredients => {
              const tempIngredients: any = ingredients;
              this.currentCocktailIngredients = tempIngredients;
            });
        }
        // console.log(this.currentCocktail);
        // console.log(this.currentCocktailIngredients);
        this.recieved = true;
        });
  
    //get current user rating for this cocktail
    if (this.authService.isAuthenticated()) {
    this.ratingService.getCocktailRatingFromUser(this.cocktailId, this.authService.getCurrentClientUser().id).subscribe(
      rating => {
        if(rating) {
        const anyRating = rating;
        this.userRating = anyRating;
        this.rating = this.userRating.userRating;
      }
      }
    );
    this.flagService.isUserTriedCocktail(this.authService.getCurrentClientUser().id, this.cocktailId).subscribe(tried => this.isTried = tried); 
  }

    //get average rating for this cocktail
    this.ratingService.getAverageCocktailRating(this.cocktailId).subscribe(
      average => {
      if(average) 
       this.currentRate = average
      });
    
  }

  openDialogIngredient(ingredientName: string) {
    let dialogRef: MatDialogRef<IngredientDialog>;
    this.cocktailApiService.searchIngredientByName(ingredientName).subscribe(
      (ingredientApi) => {
      const temp: any = ingredientApi;
      const ingredient = temp.ingredients[0];
      dialogRef = this.dialog.open(IngredientDialog, {
        width: '500px',
        height: '700px',
        data: {ingredient: ingredient, img: this.cocktailApiService.getIngredientImageMedium(ingredient.strIngredient)}     
      });
  })
  }

  //save or update user rating 
  onRatingChanged(rating){
    this.rating = rating;
    if (this.authService.isAuthenticated()) {
      if (this.userRating) {
        this.userRating.userRating = rating
        this.ratingService.updateRating(this.userRating).subscribe(rating => {
            this.ratingService.getAverageCocktailRating(this.cocktailId).subscribe(average => {
            this.currentRate = average
            });
        });
      } else {
        const changedUserRating =  {
          cocktailId: this.cocktailId,
          appUser: this.authService.getCurrentClientUser(),
          userRating: this.rating
        }
        this.ratingService.saveRating(changedUserRating).subscribe(any => {
          this.ratingService.getCocktailRatingFromUser(this.cocktailId, this.authService.getCurrentClientUser().id).subscribe(
            rating => {
              if(rating) {
              const anyRating = rating;
              this.userRating = anyRating;
              this.rating = this.userRating.userRating;
            }
            }
          );
          this.ratingService.getAverageCocktailRating(this.cocktailId).subscribe(average => {
            this.currentRate = average
          });
         // TODO check if cocktail is saved by user, send notification
         this.cocktailService.getCocktailWithId(this.cocktailId).subscribe(
           cocktail => {
            if (cocktail) { 
              this.notificationService.saveNotification(
                {
                notificationText: "rated",
                notificationCocktailId:  this.currentCocktail.id,
                notificationCocktailName: this.currentCocktail.cocktailName,
                notificationUsername: this.authService.getCurrentClientUser().username,
                userId: this.currentCocktail.appUser.id
                }
              ).subscribe();
            }
           }
         )
        });
      }
    } // end if
    else {
      this.router.navigate(['/login']);
    } // end else
  } // onRatingChange

  flagClicked() {
  //   if(!this.isTried) {
  //   this.flagService.saveFlag({
  //     cocktailId: this.cocktailId,
  //     appUser: this.authService.getCurrentClientUser(),
  //     tried: true
  //   }).subscribe(any => {
  //     this.flagService.isUserTriedCocktail(this.authService.getCurrentClientUser().id, this.cocktailId).subscribe(tried => this.isTried = tried);
  //   });
  // } else {
    this.flagService.getFlag(this.authService.getCurrentClientUser().id, this.cocktailId).subscribe(
      flag => {
        const temp:any = flag;
        if (temp === null) {
          this.flagService.saveFlag({
                cocktailId: this.cocktailId,
                appUser: this.authService.getCurrentClientUser(),
                tried: true
              }).subscribe(any => {
                this.flagService.isUserTriedCocktail(this.authService.getCurrentClientUser().id, this.cocktailId).subscribe(tried => this.isTried = tried);
              });
        } else {
          temp.tried = !temp.tried;
          this.flagService.updateFlag(temp).subscribe(
            flag => {
              const temp:any = flag;
              this.isTried = temp.tried;
            });
        }
      }
    )
  }  
}
