import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AuthService } from '../../../login/auth-service.service';
import { ReportsService } from '../../../reports.service';
import { CommentsService } from '../comments.service';
import { MatSnackBar } from '../../../../../node_modules/@angular/material';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'cocktail-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {

  @Input() comment: any;
  @Output() updateComments = new EventEmitter();
  
  constructor(public authService: AuthService, public router: Router, private reportsService: ReportsService, private commentsService: CommentsService, public snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  deleteComment(commentId: number) {
    this.commentsService.deleteComment(commentId).subscribe(any => this.updateComments.emit());
  }

  onUserClicked(userId: number){
    this.router.navigate(['/user/'+userId+'/cocktails'])
  }
  editComment(comment) {
    this.commentsService.saveComment(comment).subscribe(any => this.updateComments.emit());
  }

  reportComment(comment) {
    this.reportsService.updateReport(
      {
        reportedCommentText: comment.commentText,
        reportedCommentId: comment.id,
        cocktailId: comment.cocktailId,
        reportedUserId: comment.appUser.id,
        reportedUsername: comment.appUser.username
      }
    ).subscribe(any => this.openSnackBar("Your report has been submited", "OK"));
  }

  //show message
  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }
  
}
