import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '../../../../node_modules/@angular/forms';
import { Observable } from '../../../../node_modules/rxjs';
import { CommentsService } from './comments.service';
import { AuthService } from '../../login/auth-service.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../notification.service';
import { CocktailService } from '../../cocktail.service';

@Component({
  selector: 'cocktail-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  @Input() currentCocktailId: number;

  comments$: Observable<any>;
  currentCocktail : any;

  constructor(private commentsService: CommentsService, private authService: AuthService, private router: Router, private notificationService: NotificationService, private cocktailService: CocktailService) { }

  ngOnInit() {
    this.comments$ = this.commentsService.getAllCocktailComments(this.currentCocktailId);
    this.cocktailService.getCocktailWithId(this.currentCocktailId).subscribe(any => {
      if(any) {
      const tempCocktail = any;
      this.currentCocktail = tempCocktail;
      }
    })
  }

  //submit comment and sand notification if this is cocktail from user
  submitComment(ngForm: NgForm) {
    let temp = ngForm.value.comment.trim();
    if (this.authService.isAuthenticated() ) {
      if (temp != null && temp != '') {
      this.commentsService.saveComment({
        cocktailId: this.currentCocktailId,
        commentText: ngForm.value.comment,
        appUser: this.authService.getCurrentClientUser()
      }).subscribe(any => {
        this.comments$ = this.commentsService.getAllCocktailComments(this.currentCocktailId);
        if(this.currentCocktail) {
        this.notificationService.saveNotification(
          {
            notificationText: "commented on",
            notificationCocktailId:  this.currentCocktailId,
            notificationCocktailName: this.currentCocktail.cocktailName,
            notificationUsername: this.authService.getCurrentClientUser().username,
            userId: this.currentCocktail.appUser.id
         }
        ).subscribe();
      }
      });
      } // end inner if
    } // end outer if
    else {
      this.router.navigate(['/login']);
    } // end else
  } // end submitComment

  onUpdateComments() {
    this.comments$ = this.commentsService.getAllCocktailComments(this.currentCocktailId);
  }
}
