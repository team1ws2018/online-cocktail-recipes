import { Injectable } from '@angular/core';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import { AuthService } from '../../login/auth-service.service';

@Injectable()
export class CommentsService {
  API = 'http://localhost:8080/comments/';

  constructor(private httpClient: HttpClient, private authSevice: AuthService) { }

  getAllCocktailComments(cocktailId: number) {
    return this.httpClient.get(this.API + cocktailId);
  }

  saveComment(comment: any){
    return this.httpClient.post(this.API, comment, {headers: this.authSevice.getAuthHeaders()});
  }

  deleteComment(commentId: number) {
    return this.httpClient.delete(this.API + commentId, {headers: this.authSevice.getAuthHeaders()})
  }
}
