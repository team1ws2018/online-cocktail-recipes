import { Component, OnInit } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';
import { AuthService } from './auth-service.service';
import { NgForm } from '@angular/forms';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'cocktail-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error: Error;
  public captchaConfirmed = false;

  constructor(private authService: AuthService, private router: Router, private notificationService: NotificationService) { }

  resolved(captchaResponse: string) {
    // console.log("Resolved captcha with response " + captchaResponse);
    this.checkCaptcha(captchaResponse);

}

public checkCaptcha(code : string){
  return this.authService.checkCaptcha(code).subscribe(
    (data) => {
      this.captchaConfirmed = data;
    },
    (error) => console.log("error: " +error)
  )
}
  ngOnInit() {
  }

  onLogin(form: NgForm) {
    const username = form.value.username;
    const password = form.value.password;
    this.authService.login(username, password)
      .subscribe(
        any => {
          this.router.navigate(['/home']);
        },
        error => this.error = error
      );
  }

}
