import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '../../../node_modules/@angular/common/http';
import { Router } from '../../../node_modules/@angular/router';
import { User } from '../users/user.model';
import 'rxjs/Rx'; 
import { Observable, Subject } from 'rxjs/Rx';
import { AppUser } from '../admin-dashboard/admin-dashboard.component';
import { NotificationService } from '../notification.service';

export interface CocktailUser {
  username: string;
  roles: string[];
}

@Injectable()
export class AuthService {

  user: CocktailUser
  currentUser: AppUser;
  private authenticated = false;
  private headers;
  userIsAuthenticated = new Subject<{id:number, authenticated: boolean}>();

  constructor(private httpClient: HttpClient, private router: Router) { }

  login(username: string, password: string) {
    const base64Credential = btoa(username + ':' + password);
    const headers = new HttpHeaders({
      authorization: 'Basic ' + base64Credential
    });
    return this.httpClient
      .get<CocktailUser>('http://localhost:8080/users/user', { headers: headers })
      .do(user => {
        this.headers = headers;
        this.user = user;
        this.authenticated = true;
        this.getCurrentUser(user.username).subscribe(appUser => {
           this.currentUser = appUser; 
            this.userIsAuthenticated.next({id:this.currentUser.id, authenticated: true});
          });
         
      });
  }

  isAuthenticated() {
    return this.authenticated;
  }

  getAuthHeaders() {
    return this.headers;
  }

   // makes an object for user that's logged in
   getCurrentUser(username: string) {
    return this.httpClient
      .get<AppUser>('http://localhost:8080/users/user/' + username, { headers: this.getAuthHeaders() });
  }

  
  hasRoleAdmin() {
    if (this.user) {
      return this.user.roles.includes('ROLE_ADMIN');
    }
  }

  getUsername() {
    if (this.currentUser) {
      return this.currentUser.username;
    }
  }

  
  logout() {
    this.authenticated = false;
    this.currentUser = null;
    this.headers = null;
    this.router.navigate(['/login']);
    this.currentUser = null;
    this.userIsAuthenticated.next({id: null, authenticated: false});
  }

  public checkCaptcha(code : string) : Observable<any>{
    return this.httpClient.get("http://localhost:8080/captcha/"+code);
  }

  
  getCurrentClientUser(): AppUser {
    return this.currentUser;
  }

}
