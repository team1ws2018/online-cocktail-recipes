import { Injectable } from '@angular/core';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { Observable } from '../../node_modules/rxjs';

@Injectable()
export class CocktailApiService {

  ingredientsWithImages : any[];

  //API for Search cocktail by name
  API_SEARCH_COCKTAIL_BY_NAME = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=";
  //API for Search ingredient by name
  API_SEARCH_INGREDIENT_BY_NAME = "https://www.thecocktaildb.com/api/json/v1/1/search.php?i=";
  //API for Lookup full cocktail details by id
  API_COCKTAIL_DETAIL_BY_ID = "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=";
  //API for Lookup a random cocktail
  API_RANDOM_COCKTAIL = "https://www.thecocktaildb.com/api/json/v1/1/random.php";
  //API for Search by ingredient
  API_SEARCH_COCKTAILS_BY_INGREDIENT = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=";
  //API for Filter by Alcoholic, Non_Alcoholic
  API_FILTER_BY_ALCOHOLIC = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=";
  //API for Filter by Category
  API_FILTER_BY_CATEGORY = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=";
  //API for Filter by Glass
  API_FILTER_BY_GLASS = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=";
  //API for List the categories, glasses, ingredients or alcoholic filters
  API_LIST_CATEGORIES = "https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list";
  API_LIST_GLASSES = "https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list";
  API_LIST_INGREDIENTS = "https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list"
  API_LIST_ALCHOHOLIC = "https://www.thecocktaildb.com/api/json/v1/1/list.php?a=list"

  //images
  // add sufix from below to api image URI for diferent image size
  API_INGREDIENT_IMAGE = "https://www.thecocktaildb.com/images/ingredients/"; //image URI
  SMALL_PNG = "-Small.png"; //(100x100 pixels)
  MEDIUM_PNG =  "-Medium.png"; //(350x350 pixels)
  LARGE_PNG = ".png";  //(700x700 pixels)
  

  constructor(private httpClient: HttpClient) { }

  //Search cocktail by name
  searchCocktailByName(cocktailName: string) {
    return this.httpClient.get(this.API_SEARCH_COCKTAIL_BY_NAME + cocktailName).map(cocktailApi => this.convertCocktail(cocktailApi));;
  }

  //Search ingredient by name
  searchIngredientByName(ingredientName: string) {
    return this.httpClient.get(this.API_SEARCH_INGREDIENT_BY_NAME + ingredientName);
  }

  //Lookup full cocktail details by id
  getCocktailDetail(id: number): Observable<any> {
    return this.httpClient.get(this.API_COCKTAIL_DETAIL_BY_ID + id).map(cocktailApi => this.convertCocktail(cocktailApi));
  }

  //Lookup a random cocktail
  getRandomCocktail(): Observable<any> {
    return this.httpClient.get(this.API_RANDOM_COCKTAIL).map(cocktailApi => this.convertCocktail(cocktailApi));
  }

  //Search by ingredient
  searchCocktailsByIngredient(ingredientName: string) {
    return this.httpClient.get(this.API_SEARCH_COCKTAILS_BY_INGREDIENT + ingredientName).map(cocktailApi => {
      return this.convertCocktails(cocktailApi);
    });;
  }

  //Filter by Alcoholic, Non_Alcoholic
  filterCocktailsByAlchocolic(alcoholicType: string) {
    return this.httpClient.get(this.API_FILTER_BY_ALCOHOLIC + alcoholicType).map(cocktailApi => {
      return this.convertCocktails(cocktailApi);
    });;
  }

  //Filter by Category
  filterCocktailsByCategory(category: string) {
    return this.httpClient.get(this.API_FILTER_BY_CATEGORY + category).map(cocktailApi => {
      return this.convertCocktails(cocktailApi);
    });
  }

  //Filter by Glass
  filterCocktailsByGlass(glass: string) {
    return this.httpClient.get(this.API_FILTER_BY_GLASS + glass).map(cocktailApi => {
      return this.convertCocktails(cocktailApi);
    });;
  }

  //List the categories filter
  getListOfCategories() {
    return this.httpClient.get(this.API_LIST_CATEGORIES);
  }

  //List the glasses filter
  getListOfGlasses() {
    return this.httpClient.get(this.API_LIST_GLASSES);
  }

  //List the ingredients filter
  getListOfIngredients() {
    return this.httpClient.get(this.API_LIST_INGREDIENTS);
  }

  //List the alcoholic filter
  getListOfAlchocolic() {
    return this.httpClient.get(this.API_LIST_ALCHOHOLIC);
  }

  getIngredientImageSmall(ingredientName: string) {
    return this.API_INGREDIENT_IMAGE + ingredientName + this.SMALL_PNG;
  }

  getIngredientImageMedium(ingredientName: string) {
    return this.API_INGREDIENT_IMAGE + ingredientName + this.MEDIUM_PNG; 
  }

  getIngredientImageLarge(ingredientName: string) {
    return this.API_INGREDIENT_IMAGE + ingredientName + this.LARGE_PNG;
  }

  getIngredientsWithImages() : any[] {
    let ingredientsAPI: any;
    let ingredientsWIthImages: any = [];
    this.getListOfIngredients().subscribe(any => {
       ingredientsAPI = any;
       ingredientsAPI.drinks.forEach(ingredient => {
         ingredientsWIthImages.push({
           strIngredient: ingredient.strIngredient1,
           strThumb: this.getIngredientImageMedium(ingredient.strIngredient1)
         });
       });
    },
      error => console.log(error));
      return ingredientsWIthImages;
  }

  setIngredientsWithImages() {
    this.ingredientsWithImages = this.getIngredientsWithImages();
  }

  getIngredientsListWithImages() {
    return this.ingredientsWithImages;
  }

//helper method to convert from API ingredients to cocktail ingredients 
private convertIngredients(cocktail) {
  let ingredients = []
  for (let index = 1; index < 16; index++) { 
    if (cocktail["strIngredient" + index] === "" || cocktail["strIngredient" + index] === null) {
    
    } else {
    let ingredient = { 
      ingredientName: cocktail["strIngredient" + index],
      ingredientMeasure: "",
      ingredientImageUrl: this.getIngredientImageSmall(cocktail["strIngredient" + index])
    }

    if (cocktail['strMeasure' + index] === "" ||  cocktail['strMeasure' + index] === null) {
    
    } else {
      ingredient.ingredientMeasure = cocktail['strMeasure' + index];
    }
    ingredients.push(ingredient);
  }
  }
  return ingredients;
}

//helper method for converting cocktail from api to database model
private convertCocktail(cocktailApi) {
  const tempCocktailApi: any = cocktailApi;
        if (tempCocktailApi.drinks !== null) {
          const cocktail = tempCocktailApi.drinks[0];
          return {
            id: cocktail.idDrink,
            cocktailName: cocktail.strDrink,
            cocktailCategory: cocktail.strCategory,
            cocktailType: cocktail.strAlcoholic,
            cocktailScreenshotUrl: cocktail.strDrinkThumb,
            glassType: cocktail.strGlass,
            instructions: cocktail.strInstructions,
            visibilityControl: null,
            appUser: null,
            ingredients: this.convertIngredients(cocktail)
          };
        } else {
           return null;
        }
}

//helper method to conver list of API cocktails to list of database cocktails
private convertCocktails(cocktailsApi) {
  const tempCocktailsApi: any = cocktailsApi;
        if (tempCocktailsApi.drinks !== null) {
          const cocktails = tempCocktailsApi.drinks;
          return cocktails.map(cocktail => { return {
            id: cocktail.idDrink,
            cocktailName: cocktail.strDrink,
            cocktailScreenshotUrl: cocktail.strDrinkThumb,
          };
        })
        } else {
           return null;
        }
}
}