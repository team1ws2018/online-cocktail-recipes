import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cocktail-home-user-cocktail-card',
  templateUrl: './home-user-cocktail-card.component.html',
  styleUrls: ['./home-user-cocktail-card.component.css']
})
export class HomeUserCocktailCardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
