import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeUserCocktailCardComponent } from './home-user-cocktail-card.component';

describe('HomeUserCocktailCardComponent', () => {
  let component: HomeUserCocktailCardComponent;
  let fixture: ComponentFixture<HomeUserCocktailCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeUserCocktailCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeUserCocktailCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
