import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cocktail-ingredient-card',
  templateUrl: './ingredient-card.component.html',
  styleUrls: ['./ingredient-card.component.css']
})
export class IngredientCardComponent implements OnInit {

  @Input() ingredient: {strThumb, strIngredient};
  @Output() onIgredientClick  =  new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  emitClicked(ingredient: any) {
    this.onIgredientClick.emit(ingredient);
  }

}
