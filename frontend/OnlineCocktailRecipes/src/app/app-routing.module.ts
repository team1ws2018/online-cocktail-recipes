import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardAccessDeniedService } from './auth-guard-access-denied.service';
import { SignUpComponent } from './sign-up/sign-up.component';
import { CreateCocktailComponent } from './create-cocktail/create-cocktail.component';
import { ForgottenCredentialsComponent } from './forgotten-credentials/forgotten-credentials.component';
import { CocktailDetailsComponent } from './cocktail-details/cocktail-details.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';
import { AdminReportsListComponent } from './admin-dashboard/admin-reports-list/admin-reports-list.component';
import { FavoritesComponent } from './my-recipes/favorites/favorites.component';
import { MyRecipesComponent } from './my-recipes/my-recipes.component';
import { AuthGuard } from './auth-guard.service';
import { UsersComponent } from './users/users.component';
import { AuthGuardUserBlockedService } from './auth-guard-user-blocked.service';

const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuardAccessDeniedService] },
  { path: 'signup', component: SignUpComponent, canActivate: [AuthGuardAccessDeniedService] },
  { path: 'create', component: CreateCocktailComponent, canActivate: [AuthGuardUserBlockedService], data: {expectedRole: 'user'} },
  { path: 'login/forgotten', component: ForgottenCredentialsComponent},
  { path: 'cocktail/:id', component: CocktailDetailsComponent},
  { path: 'dashboard', component: AdminDashboardComponent, canActivate: [AuthGuard], data: { expectedRole: 'admin'}},
  { path: 'user/:id', component: UserProfileComponent,canActivate: [AuthGuard], data: {expectedRole: 'user'} },
  { path: 'reports', component: AdminReportsListComponent, canActivate: [AuthGuard], data: { expectedRole: 'admin'}  },
  { path: 'favorites', component: FavoritesComponent, canActivate: [AuthGuard], data: { expectedRole: 'user'} },
  { path: 'myrecipes', component: MyRecipesComponent, canActivate: [AuthGuard], data: { expectedRole: 'user'} },
  { path: 'user/:id/cocktails', component: UsersComponent },
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true } )],
  exports: [RouterModule]
})

export class AppRoutingModule {

 }
