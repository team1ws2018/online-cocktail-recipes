import { Component, OnInit } from '@angular/core';
import { AuthService } from '../login/auth-service.service';
import { EmailService } from '../email.service';
import { Router } from '../../../node_modules/@angular/router';
import { UsersService } from '../users/users.service';
import { NgForm } from '../../../node_modules/@angular/forms';
import { User } from '../users/user.model';
import { HttpHeaders } from '../../../node_modules/@angular/common/http';

@Component({
  selector: 'cocktail-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  public captchaConfirmed = false;
  error: Error;
  notification: string;

  constructor(private authService : AuthService, private router: Router, private emailService: EmailService, public usersService: UsersService) { }

  resolved(captchaResponse: string) {
    this.checkCaptcha(captchaResponse);

}

public checkCaptcha(code : string){
  return this.authService.checkCaptcha(code).subscribe(
    (data) => {
      this.captchaConfirmed = data;
    },
    (error) => console.log("error je: " +error)
  )
}
 

  ngOnInit() {
  }

  onSignUp(ngForm: NgForm) {
    let userToSave = new User(ngForm.value.username, ngForm.value.password, ngForm.value.email, false);
    userToSave = JSON.parse(JSON.stringify(userToSave));
    const base64Credential = btoa("admin" + ':' + "admin");
    const headers = new HttpHeaders({
      authorization: 'Basic ' + base64Credential
    });
    this.usersService.registerUser(userToSave, headers).subscribe(
         any => {
          this.router.navigate(["/login"]);
        },        
        error => this.error = error
    );
  
  }

}
