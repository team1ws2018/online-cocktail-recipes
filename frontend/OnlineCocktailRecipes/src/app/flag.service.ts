import { Injectable } from '@angular/core';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { Observable } from '../../node_modules/rxjs';
import { AuthService } from './login/auth-service.service';

@Injectable()
export class FlagService {
  API = "http://localhost:8080/flags/";

  constructor(private httpCLicent: HttpClient, private authService: AuthService) { }


    saveFlag(flag): Observable<any> {
      return this.httpCLicent.post(this.API, flag, {headers: this.authService.getAuthHeaders()})
    }
    
    //returns observable with boolean value (is user tried this cocktail)
    isUserTriedCocktail(userId, cocktailId):Observable<boolean> {
      return this.httpCLicent.get<boolean>(this.API + "tried/user/"+ userId + "/cocktail/" + cocktailId, {headers: this.authService.getAuthHeaders()});
    }

    getFlag(userId, cocktailId) {
      return this.httpCLicent.get<boolean>(this.API + "user/"+ userId + "/cocktail/" + cocktailId, {headers: this.authService.getAuthHeaders()});
    }
    
    updateFlag(flag) {
      return this.httpCLicent.post(this.API, flag, {headers: this.authService.getAuthHeaders()})
    }

}
