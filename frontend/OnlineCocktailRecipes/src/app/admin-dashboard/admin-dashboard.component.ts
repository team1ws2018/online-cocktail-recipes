import { Component, OnInit, ViewChild, Inject, Output, EventEmitter } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatSlideToggleChange, MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatDatepickerInputEvent} from '@angular/material';
import { UsersService } from '../users/users.service';
import { SelectionModel } from '../../../node_modules/@angular/cdk/collections';
import { Observable } from '../../../node_modules/rxjs';
import { CocktailService } from '../cocktail.service';
import { FormControl } from '../../../node_modules/@angular/forms';

export interface AppUser {
  id: number;
  roles: string[];
  username: string;
  password: string;
  email: string;
  blocked: boolean;
  blockedUntil: Date;
  emailNotification: boolean;
}

export interface DialogData {
  data: AppUser[];
}

export interface BlockUntilDialogData {
  data: AppUser;
}

@Component({
  selector: 'cocktail-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  displayedColumns: string[] = ['select', 'id', 'username', 'email', 'blocked', 'blockedUntil'];
  dataSource: MatTableDataSource<AppUser>;
  selection = new SelectionModel<AppUser>(true, []);
  deleteUsersDialog: MatDialogRef<DialogDeleteUsers>;
  blockUntilDialog: MatDialogRef<DialogBlockUntil>;
  previousDataLength: number;
  usersStatistic: any;
  cocktailsStatistic: any;
  minDate = new Date();


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private usersService: UsersService, public snackBar: MatSnackBar, public dialog: MatDialog, private cocktailService: CocktailService) {
  
  }

  ngOnInit() {
    //get all users fil data source for table 
    this.usersService.getAllUser().subscribe(
      users => {
        this.dataSource = new MatTableDataSource<AppUser>(users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.previousDataLength = this.dataSource.data.length;
      }
    );

  // get statistics data for users and cocktails
   this.usersService.getUsersStatistics().subscribe(statistic => this.usersStatistic = statistic);
   this.cocktailService.getCocktailsStatistic().subscribe(statistic => this.cocktailsStatistic = statistic);
   
  }

    // Whether the number of selected elements matches the total number of rows. 
    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
  
  //Selects all rows if they are not all selected; otherwise clear selection.
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

//filter table
applyFilter(filterValue: string) {
  this.dataSource.filter = filterValue.trim().toLowerCase();
  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}

  //When admin toggle user status(blocked status)
  onStatusChange(status: MatSlideToggleChange, appUser: AppUser) {

    this.usersService.getUserWithId(appUser.id).subscribe(
      user => {
        //toggle blocked status
        user.blocked = !status.checked;
        //set blocked until to null(will update that later)
        user.blockedUntil = null;
        //update user in database
        this.usersService.updateUser(user).subscribe(user => {
          //refresh page data
          this.usersService.getAllUser().subscribe(
            users => {
              this.dataSource = new MatTableDataSource<AppUser>(users);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              const previousSelection: AppUser[] = Object.assign([], this.selection.selected);
              this.selection.clear();
              this.dataSource.data.forEach(row => {
                const result = previousSelection.filter(user => user.id === row.id);
               if(result.length > 0) {
                 this.selection.select(row);
               }
              })
            }
          );
          const isBlockedStr = user.blocked?'blocked':'active'
          const msg = 'User '+ user.username + ' is now ' + isBlockedStr + '!';
          this.openSnackBar(msg, 'OK');
          this.usersService.getUsersStatistics().subscribe(statistic => this.usersStatistic = statistic);
        });
      }
    );
    //open block until dialog only if user is already blocked
    if(!appUser.blocked) {
    this.openBlockUntilDialog(appUser);
  }
  }
  
  //show message
  private openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2500,
    });
  }

  private findAllUsers() {
    this.usersService.getAllUser().subscribe(
      users => {
        this.dataSource = new MatTableDataSource<AppUser>(users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  openDeleteUserDialog(): void {
    this.deleteUsersDialog = this.dialog.open(DialogDeleteUsers, {
      width: '300px',
      data: this.selection.selected
    });

    this.deleteUsersDialog.afterClosed().subscribe(result => {
      this.usersService.getAllUser().subscribe(
        users => {
          if(users.length !== this.previousDataLength) {
            this.findAllUsers();
            this.selection = new SelectionModel<AppUser>(true, []);
            this.previousDataLength = this.dataSource.data.length;
            this.usersService.getUsersStatistics().subscribe(statistic => this.usersStatistic = statistic);
            this.cocktailService.getCocktailsStatistic().subscribe(statistic => this.cocktailsStatistic = statistic);
          }
        }
      ) 
    });
  }

  openBlockUntilDialog(user: AppUser): void {
    this.blockUntilDialog = this.dialog.open(DialogBlockUntil, {
      width: '300px',
      data: user
    });
    this.blockUntilDialog.afterClosed().subscribe(result => {
        this.usersService.getAllUser().subscribe(
          users => {
            this.dataSource = new MatTableDataSource<AppUser>(users);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            const previousSelection: AppUser[] = Object.assign([], this.selection.selected);
            this.selection.clear();
            this.dataSource.data.forEach(row => {
              const result = previousSelection.filter(user => user.id === row.id);
             if(result.length > 0) {
               this.selection.select(row);
             }
            })
            
            if (user.blockedUntil) {
            const date = new Date(user.blockedUntil);
            const msg = user.username + " will be activated on " + date.toLocaleDateString("en-US");
            this.openSnackBar(msg, "OK");
          }
          }
        );
    });
  }

  //on blocked until date change update user with new informations
  blockedUntilChanged(user: AppUser, event: MatDatepickerInputEvent<Date>) {
    user.blockedUntil = event.value;
    this.usersService.updateUser(user).subscribe(user => {
      const date = new Date(user.blockedUntil);
      const msg = user.username + " will be activated on " + date.toLocaleDateString("en-US");
      this.openSnackBar(msg, "OK");
    });
  }

}

@Component({
  selector: 'dialog-overview-example-dialog',
  template: ` <h2 mat-dialog-title>Are you sure you want to delete?</h2>
              <mat-dialog-content>
              <mat-list>
              <mat-list-item *ngFor="let user of data"> {{ user.username }} </mat-list-item>
             </mat-list>
             </mat-dialog-content>
              <mat-dialog-actions>
              <button mat-button mat-dialog-close  tabindex="1">NO</button>
              <!-- The mat-dialog-close directive optionally accepts a value as a result for the dialog. -->
              <button mat-button tabindex="0" (click)="deleteUsers(data)">YES</button>
              </mat-dialog-actions> `
})
export class DialogDeleteUsers {

  constructor(
    public dialogRef: MatDialogRef<DialogDeleteUsers>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private usersService: UsersService) {}

    deleteUsers(users) {
      this.usersService.deleteAllUsers(users).subscribe(any => this.dialogRef.close());
    }

}

@Component({
  selector: 'dialog-block-until',
  template: ` <h2 mat-dialog-title>Select date</h2>
              <mat-dialog-content>
              {{ "When do you want " + data.username + " to be active again?" }} 
              <br>
              <mat-form-field>
                <input disabled [min]="minDate" matInput [matDatepicker]="picker3" placeholder="Choose date" (dateChange)="dateChanged($event)">
                <mat-datepicker-toggle matSuffix [for]="picker3"></mat-datepicker-toggle>
                <mat-datepicker disabled="false"  touchUi="true" #picker3></mat-datepicker>
              </mat-form-field>
              </mat-dialog-content>
              <mat-dialog-actions>
              <button mat-button mat-dialog-close>CANCEL</button>
              <button mat-button (click)="blockUserUntil(data)" mat-dialog-close>OK</button>
              </mat-dialog-actions> `
})
export class DialogBlockUntil implements OnInit {
  date: FormControl;
  blockedUntil: Date
  minDate = new Date();

  constructor(
    public dialogRef: MatDialogRef<DialogBlockUntil>,
    @Inject(MAT_DIALOG_DATA) public data: BlockUntilDialogData, private usersService: UsersService) {}

    ngOnInit(): void {
      this.date = new FormControl(new Date());
      console.log(this.data);
    }

    blockUserUntil(data) {
      data.blocked = !data.blocked;
      data.blockedUntil = this.blockedUntil;
      this.usersService.updateUser(data).subscribe();
      this.dialogRef.close(data);
    }

    dateChanged(event: MatDatepickerInputEvent<Date>) {
      this.blockedUntil = event.value;
    }
}
