import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../reports.service';
import { Observable } from '../../../../node_modules/rxjs';
import { AppUser } from '../admin-dashboard.component';
import { UsersService } from '../../users/users.service';
import { CommentsService } from '../../cocktail-details/comments/comments.service';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'cocktail-admin-reports-list',
  templateUrl: './admin-reports-list.component.html',
  styleUrls: ['./admin-reports-list.component.css']
})
export class AdminReportsListComponent implements OnInit {

  reports$: Observable<any>;

  constructor(private reportsService: ReportsService, private usersService: UsersService, private commentsService: CommentsService, private router: Router) { }

  ngOnInit() {
    this.reports$ = this.reportsService.getAllReports();
  }

  resolveReport(report: any) {
    report.resolved = !report.resolved;
    this.reportsService.updateReport(report).subscribe();
  }

  blockUser(user: AppUser) {
    user.blocked = !user.blocked;
    this.usersService.updateUser(user).subscribe();
  }

  deleteReport(reportId: number) {
    this.reportsService.deleteReport(reportId).subscribe(any => this.reports$ = this.reportsService.getAllReports());
  }

  deleteComment(commentId: number) {
    this.commentsService.deleteComment(commentId).subscribe();
  }

  goToCocktailDetailPage(cocktailId: number) {
    this.router.navigate([ 'cocktail/'+cocktailId]);
  }
}
