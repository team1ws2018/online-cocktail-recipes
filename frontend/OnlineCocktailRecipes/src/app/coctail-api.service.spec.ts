import { TestBed, inject } from '@angular/core/testing';

import { CocktailApiService } from './coctail-api.service';

describe('CoctailApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CocktailApiService]
    });
  });

  it('should be created', inject([CocktailApiService], (service: CocktailApiService) => {
    expect(service).toBeTruthy();
  }));
});
