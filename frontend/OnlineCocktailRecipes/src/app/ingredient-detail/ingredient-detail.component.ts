import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cocktail-ingredient-detail',
  templateUrl: './ingredient-detail.component.html',
  styleUrls: ['./ingredient-detail.component.css']
})
export class IngredientDetailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
