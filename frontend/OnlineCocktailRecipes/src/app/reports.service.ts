import { Injectable } from '@angular/core';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { AuthService } from './login/auth-service.service';

@Injectable()
export class ReportsService {
  API = 'http://localhost:8080/reports/';

  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  getAllReports() {
    return this.httpClient.get(this.API, {headers: this.authService.getAuthHeaders()});
  }

  updateReport(report: any) {
    return this.httpClient.post(this.API, report,  {headers: this.authService.getAuthHeaders()});
  }

  deleteReport(reportId: number) {
    return this.httpClient.delete(this.API + reportId, {headers: this.authService.getAuthHeaders()});
  }

}
