import { Injectable } from '@angular/core';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { AuthService } from './login/auth-service.service';
import { Subject, Observable } from '../../node_modules/rxjs';

@Injectable()
export class NotificationService {

  API = 'http://localhost:8080/notifications/';

  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  saveNotification(notification) {
    return this.httpClient.post(this.API, notification, {headers: this.authService.getAuthHeaders()})
  }

  getUserNotifications(userId: number): Observable<any[]> {
    return this.httpClient.get<any[]>(this.API + "user/" + userId, {headers: this.authService.getAuthHeaders()})
  }

  deleteNotification(notificationId: number) {
    return this.httpClient.delete(this.API + notificationId, {headers: this.authService.getAuthHeaders()});
  }

}
